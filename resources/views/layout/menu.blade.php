<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item me-auto"><a class="navbar-brand" href="{{ url('/dashboard') }}">
                    <img class="img-fluid brand-logo" alt="Responsive image"
                        src="{{ asset('app-assets/images/logo/rsiaputi_logo.png') }}" width="23px" height="23px">
                    <h2 class="brand-text">SIMPEG</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse"><i
                        class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i
                        class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc"
                        data-ticon="disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="navigation-header"><span data-i18n="Menu Karyawan">Menu Utama</span><i
                    data-feather="more-horizontal"></i>
            </li>
            <li class="nav-item {{ $menu == 'dashboard' ? 'active' : '' }}"><a class="d-flex align-items-center"
                    href="{{ url('/dashboard') }}"><i data-feather="home"></i><span class="menu-title text-truncate"
                        data-i18n="Dashboard">Dashboard</span></a>
            </li>
            <li class="nav-item {{ $menu == 'dashboard-direksi' ? 'active' : '' }}"><a class="d-flex align-items-center"
                    href="{{ url('/dashboard-direksi') }}"><i data-feather="home"></i><span class="menu-title text-truncate"
                        data-i18n="Dashboard Direksi">Dashboard Direksi</span></a>
            </li>
            <li class="nav-item {{ $menu == 'personal-data' ? 'active' : '' }}"><a class="d-flex align-items-center"
                    href="{{ url('/personal-data') }}"><i data-feather="user"></i><span
                        class="menu-title text-truncate" data-i18n="Personal Data">Personal Data</span></a>
            </li>

            <li class="navigation-header"><span data-i18n="Data Master">Data Pengelolaan</span><i
                    data-feather="more-horizontal"></i>
            </li>
            <li class="nav-item {{ $menu == 'data-karyawan' ? 'active' : '' }}"><a class="d-flex align-items-center"
                    href="{{ url('/data-karyawan') }}"><i data-feather="users"></i><span
                        class="menu-title text-truncate" data-i18n="Data Karyawan">Data Karyawan</span></a>
            </li>
            <li class="nav-item {{ $menu == 'jadwal-karyawan' ? 'active' : '' }}"><a
                    class="d-flex align-items-center" href="{{ url('/jadwal-karyawan') }}"><i
                        data-feather="sliders"></i><span class="menu-title text-truncate"
                        data-i18n="Jadwal Karyawan">Jadwal Karyawan</span></a>
            </li>
            <li class="nav-item {{ $menu == 'dokumen-legalitas' ? 'active' : '' }}"><a
                    class="d-flex align-items-center" href="{{ url('/dokumen-legalitas') }}"><i
                        data-feather="file"></i><span class="menu-title text-truncate"
                        data-i18n="Dokumen Legalitas">Dokumen Legalitas</span></a>
            </li>
            <li class="nav-item {{ $menu == 'data-presensi' ? 'active' : '' }}"><a class="d-flex align-items-center"
                    href="{{ url('/data-presensi') }}"><i data-feather="user-check"></i><span
                        class="menu-title text-truncate" data-i18n="Presensi">Presensi</span></a>
            </li>
            <li class="nav-item {{ $menu == 'data-gaji' ? 'active' : '' }}"><a class="d-flex align-items-center"
                    href="{{ url('/data-gaji') }}"><i data-feather="dollar-sign"></i><span
                        class="menu-title text-truncate" data-i18n="Pengelolaan Gaji">Pengelolaan Gaji</span></a>
            </li>

            <li class="navigation-header"><span data-i18n="Data Master">Data Master</span><i
                    data-feather="more-horizontal"></i>
            </li>
            <li class="nav-item {{ $menu == 'data-unit' ? 'active' : '' }}"><a class="d-flex align-items-center"
                    href="{{ url('/data-unit') }}"><i data-feather="database"></i><span
                        class="menu-title text-truncate" data-i18n="Data Unit">Data Unit</span></a>
            </li>
            <li class="nav-item {{ $menu == 'data-dokumen-legalitas' ? 'active' : '' }}"><a
                    class="d-flex align-items-center" href="{{ url('/data-dokumen-legalitas') }}"><i
                        data-feather="database"></i><span class="menu-title text-truncate"
                        data-i18n="Data Dokumen Legalitas">Data Dok.
                        Legalitas</span></a>
            </li>
            <li class="nav-item {{ $menu == 'data-kategori' ? 'has-sub sidebar-group-active open' : '' }}"><a
                    class="d-flex align-items-center" href="#"><i data-feather="database"></i><span
                        class="menu-title text-truncate" data-i18n="Dashboards">Data Kategori</a>
                <ul class="menu-content">
                    <li class="{{ $submenu == 'data-diklat' ? 'active' : '' }}"><a class="d-flex align-items-center"
                            href="{{ url('/data-pelatihan-diklat') }}"><i data-feather="circle"></i><span
                                class="menu-item text-truncate" data-i18n="Analytics">Pelatihan & Diklat</span></a>
                    </li>
                    <li class="{{ $submenu == 'data-profesi' ? 'active' : '' }}"><a class="d-flex align-items-center"
                            href="{{ url('/data-profesi') }}"><i data-feather="circle"></i><span
                                class="menu-item text-truncate" data-i18n="Profesi">Profesi</span></a>
                    </li>
                    <li class="{{ $submenu == 'data-tunjangan' ? 'active' : '' }}"><a
                            class="d-flex align-items-center" href="{{ url('/data-tunjangan') }}"><i
                                data-feather="circle"></i><span class="menu-item text-truncate"
                                data-i18n="Tunjangan">Tunjangan</span></a>
                    </li>
                    <li class="{{ $submenu == 'data-jabatan' ? 'active' : '' }}"><a class="d-flex align-items-center"
                            href="{{ url('/data-jabatan') }}"><i data-feather="circle"></i><span
                                class="menu-item text-truncate" data-i18n="Jabatan">Jabatan</span></a>
                    </li>
                    <li class="{{ $submenu == 'data-cuti' ? 'active' : '' }}"><a class="d-flex align-items-center"
                        href="{{ url('/data-cuti') }}"><i data-feather="circle"></i><span
                            class="menu-item text-truncate" data-i18n="Cuti">Cuti</span></a>
                </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
