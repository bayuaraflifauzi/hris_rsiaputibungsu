@extends('layout.main')

@section('custom-css')
    <style></style>
@endsection

@section('custom-js')
    <script>
        $(function() {
            'use strict';
        })
    </script>
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-8 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">{!! $title !!}</h2>
                </div>
            </div>
        </div>
        <div class="content-header-right text-md-end col-md-4 col-12 d-md-block d-none">
            <button class="btn btn-outline-primary" type="button" onclick="history.back()">Kembali</button>
            <a href="{{ asset('/Slip.pdf') }}" class="btn btn-primary" download="Slip Gaji"><i data-feather="file" class="me-25"></i>
                <span>Slip Gaji</span></a>
        </div>
    </div>

    <div class="content-body">
        <div class="row" id="basic-table">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border">
                        <h4 class="title mb-0 text-uppercase">Data Karyawan</h4>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li>
                                    <a data-action="collapse" class=""><i data-feather="chevron-up"
                                            class="me-25"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <div class="row mt-2">
                                <div class="mb-1 col-md-4">
                                    <label class="form-label">NIP</label>
                                    <input type="text" class="form-control" value="5678" readonly="">
                                </div>
                                <div class="mb-1 col-md-4">
                                    <label class="form-label">Nama Karyawan</label>
                                    <input type="text" class="form-control" value="Firman Herdiansyah" readonly="">
                                </div>
                                <div class="mb-1 col-md-4">
                                    <label class="form-label">Pendidikan Terakhir</label>
                                    <input type="text" class="form-control" value="Sarjana 1 (S1)" readonly="">
                                </div>
                                <div class="mb-1 col-md-4">
                                    <label class="form-label">Unit</label>
                                    <input type="text" class="form-control" value="Unit Rawat Inap" readonly="">
                                </div>
                                <div class="mb-1 col-md-4">
                                    <label class="form-label">Jabatan</label>
                                    <input type="text" class="form-control" value="Pelaksana" readonly="">
                                </div>
                                <div class="mb-1 col-md-4">
                                    <label class="form-label">Profesi</label>
                                    <input type="text" class="form-control" value="Perawat" readonly="">
                                </div>
                                <div class="mb-1 col-md-12">
                                    <label class="form-label">Total Gaji Periode ini</label>
                                    <input type="text" class="form-control" value="Rp 11.000.000" readonly="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="presensi-tab" data-bs-toggle="tab" href="#presensi"
                                    aria-controls="presensi" role="tab" aria-selected="true">Log Presensi</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tunjangan-tab" data-bs-toggle="tab" href="#tunjangan"
                                    aria-controls="tunjangan" role="tab" aria-selected="false">Tunjangan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="slip-tab" data-bs-toggle="tab" href="#slip"
                                    aria-controls="slip" role="tab" aria-selected="false">Potongan Lain</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="presensi" aria-labelledby="presensi-tab" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="datatables-basic table">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Sesi</th>
                                                <th>Waktu Sesi Masuk</th>
                                                <th>Waktu Sesi Pulang</th>
                                                <th>Waktu Masuk</th>
                                                <th>Waktu Pulang</th>
                                                <th>Selisih Waktu</th>
                                                <th>Keterangan</th>
                                                <th>Lembur</th>
                                                <th>Total Waktu Lembur</th>
                                                <th>Total Upah Lembur</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Shift Pagi</td>
                                                <td>08:00:00</td>
                                                <td>14:00:00</td>
                                                <td>04-03-2022 07:35:43</td>
                                                <td>04-03-2022 14:05:12</td>
                                                <td>0 Menit</td>
                                                <td>-</td>
                                                <td><span class="badge bg-dark">Tidak</span></td>
                                                <td>0 Menit</td>
                                                <td>Rp 0</td>
                                                <td>
                                                    <button type="button"
                                                        class="btn btn-icon rounded-circle btn-outline-dark"
                                                        data-toggle="tooltip" data-placement="top" title="Edit">
                                                        <i data-feather="edit-2"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="tunjangan" aria-labelledby="tunjangan-tab" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="datatables-basic table" id="jadwal-kerja">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Tunjangan</th>
                                                <th>Berdasarkan Tunjangan</th>
                                                <th>Keterangan</th>
                                                <th>Nominal</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Tunjangan Kesehatan</td>
                                                <td>Jabatan</td>
                                                <td>Jabatan 1</td>
                                                <td>Rp 1.000.000</td>
                                                <td>
                                                    <button type="button"
                                                        class="btn btn-icon rounded-circle btn-outline-dark"
                                                        data-toggle="tooltip" data-placement="top" title="Edit">
                                                        <i data-feather="edit-2"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Tunjangan Kesehatan</td>
                                                <td>Pendidikan</td>
                                                <td>Sarjana 1 (S1)</td>
                                                <td>Rp 1.000.000</td>
                                                <td>
                                                    <button type="button"
                                                        class="btn btn-icon rounded-circle btn-outline-dark"
                                                        data-toggle="tooltip" data-placement="top" title="Edit">
                                                        <i data-feather="edit-2"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="slip" aria-labelledby="slip-tab" role="tabpanel">
                                <div class="table-responsive">
                                    <table class="datatables-basic table" id="jadwal-kerja">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Keterangan Potongan</th>
                                                <th>Nominal</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Punishment Keterlambatan</td>
                                                <td>Rp 500.000</td>
                                                <td>
                                                    <button type="button"
                                                        class="btn btn-icon rounded-circle btn-outline-dark"
                                                        data-toggle="tooltip" data-placement="top" title="Edit">
                                                        <i data-feather="edit-2"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
