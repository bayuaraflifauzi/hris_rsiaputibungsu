@extends('layout.main')

@section('custom-css')
    <style></style>
@endsection

@section('custom-js')
    <script>
        $(function() {
            'use strict';
        })
    </script>
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">{{ $title }}</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Beranda</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Data Presensi</a>
                            </li>
                            <li class="breadcrumb-item active">{{ $title }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <div class="row" id="basic-table">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-end pb-0">
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#add">
                            <i data-feather="plus" class="me-25"></i>
                            <span>Tambah Periode</span>
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="datatables-basic table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Awal Periode</th>
                                        <th>Selesai Periode</th>
                                        <th>Jumlah Orang</th>
                                        <th>Total Rupiah Gaji</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>26 Januari 2022</td>
                                        <td>25 Februari 2022</td>
                                        <td>123</td>
                                        <td>Rp 172.000.000</td>
                                        <td>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-info"
                                                data-toggle="tooltip" data-placement="top" title="Detail"
                                                onclick="window.location = '{{ url('/data-gaji/detail-periode') }}'">
                                                <i data-feather="eye"></i>
                                            </button>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-danger"
                                                data-toggle="tooltip" data-placement="top" title="Hapus">
                                                <i data-feather="trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade text-start" id="add" tabindex="-1" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel33">Tambah Data</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="#">
                    <div class="modal-body">
                        <div class="mb-1">
                            <label class="form-label">Awal Periode</label>
                            <input type="date" placeholder="Pilih awal periode" class="form-control" />
                        </div>

                        <div class="mb-1">
                            <label class="form-label">Selesai Periode</label>
                            <input type="date" placeholder="Pilih selesai periode" class="form-control" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
