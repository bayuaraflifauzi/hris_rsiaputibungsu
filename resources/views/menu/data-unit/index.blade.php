@extends('layout.main')

@section('custom-css')
    <style></style>
@endsection

@section('custom-js')
    <script>
        $(function() {
            'use strict';
        })
    </script>
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-md-12">
                    <h2 class="content-header-title float-start mb-0">{{ $title }}</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Beranda</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Data Master</a>
                            </li>
                            <li class="breadcrumb-item active">{{ $title }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
            <div class="mb-1 breadcrumb-right">
                <div class="dropdown">
                    <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button"
                        data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                            data-feather="grid"></i></button>
                    <div class="dropdown-menu dropdown-menu-end">
                        <a class="dropdown-item" href="#"><i class="me-1" data-feather="filter"></i><span
                                class="align-middle">Filter</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <div class="row" id="basic-table">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-end pb-0">
                        <button type="button" class="btn btn-outline-primary me-1">
                            <i data-feather="file" class="me-25"></i>
                            <span>Eksport</span>
                        </button>
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#add">
                            <i data-feather="plus" class="me-25"></i>
                            <span>Tambah Data</span>
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="datatables-basic table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Unit</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Unit 1</td>
                                        <td>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-dark"
                                                data-toggle="tooltip" data-placement="top" title="Edit">
                                                <i data-feather="edit-2"></i>
                                            </button>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-danger"
                                                data-toggle="tooltip" data-placement="top" title="Hapus">
                                                <i data-feather="trash"></i>
                                            </button>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-warning"
                                                data-toggle="tooltip" data-placement="top" title="Pengaturan" onclick="window.location = '{{ url('/data-unit/settings') }}'">
                                                <i data-feather="settings"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Unit 2</td>
                                        <td>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-dark"
                                                data-toggle="tooltip" data-placement="top" title="Edit">
                                                <i data-feather="edit-2"></i>
                                            </button>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-danger"
                                                data-toggle="tooltip" data-placement="top" title="Hapus">
                                                <i data-feather="trash"></i>
                                            </button>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-warning"
                                                data-toggle="tooltip" data-placement="top" title="Pengaturan" onclick="window.location = '{{ url('/data-unit/settings') }}'">
                                                <i data-feather="settings"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Unit 3</td>
                                        <td>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-dark"
                                                data-toggle="tooltip" data-placement="top" title="Edit">
                                                <i data-feather="edit-2"></i>
                                            </button>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-danger"
                                                data-toggle="tooltip" data-placement="top" title="Hapus">
                                                <i data-feather="trash"></i>
                                            </button>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-warning"
                                                data-toggle="tooltip" data-placement="top" title="Pengaturan" onclick="window.location = '{{ url('/data-unit/settings') }}'">
                                                <i data-feather="settings"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade text-start" id="add" tabindex="-1" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel33">Tambah Data</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="#">
                    <div class="modal-body">
                        <label class="form-label">Nama Unit</label>
                        <div class="mb-1">
                            <input type="text" placeholder="Masukkan nama unit" class="form-control" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
