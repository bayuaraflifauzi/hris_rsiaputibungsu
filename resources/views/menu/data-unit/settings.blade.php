@extends('layout.main')

@section('custom-css')
    <style></style>
@endsection

@section('custom-js')
    <script>
        $(function() {
            'use strict';
        })
    </script>
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">{{ $title }}</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Beranda</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Data Master</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Data Unit 1</a>
                            </li>
                            <li class="breadcrumb-item active">{{ $title }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
            <div class="mb-1 breadcrumb-right">
                <button class="btn btn-outline-primary" type="button" onclick="history.back()">Kembali</button>
            </div>
        </div>
    </div>

    <div class="content-body">
        <div class="row">
            <!-- left menu section -->
            <div class="col-md-3 mb-2 mb-md-0">
                <ul class="nav nav-pills flex-column nav-left">
                    <li class="nav-item">
                        <a class="nav-link active" id="account-pill-dokumen-peryaratan" data-bs-toggle="pill"
                            href="#account-vertical-dokumen-peryaratan" aria-expanded="true">
                            <i data-feather="file" class="font-medium-3 me-1"></i>
                            <span class="fw-bold">Persyaratan Unit</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="account-pill-password" data-bs-toggle="pill"
                            href="#account-vertical-jadwal" aria-expanded="false">
                            <i data-feather="lock" class="font-medium-3 me-1"></i>
                            <span class="fw-bold">Jadwal Kerja</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="account-pill-password" data-bs-toggle="pill"
                            href="#account-vertical-tunjangan" aria-expanded="false">
                            <i data-feather="lock" class="font-medium-3 me-1"></i>
                            <span class="fw-bold">Tunjangan</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!--/ left menu section -->

            <!-- right content section -->
            <div class="col-md-9">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="account-vertical-dokumen-peryaratan"
                        aria-labelledby="account-pill-dokumen-peryaratan" aria-expanded="true">
                        <div class="card">
                            <div class="card-header py-1">
                                <h3 class="title mb-0">Persyaratan Unit</h3>
                                <div class="d-flex justify-content-end">
                                    <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                        data-bs-target="#add-dokumen">
                                        <i data-feather="plus" class="me-25"></i>
                                        <span>Tambah Data</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h3 class="title">Dokumen Persyaratan</h3>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="datatables-basic table" id="dokumen-legalitas">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Dokumen</th>
                                                <th>Keterangan</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Ijasah D3</td>
                                                <td>Jurusan Keperawatan</td>
                                                <td><span class="badge bg-success">Wajib</span></td>
                                                <td>
                                                    <button type="button"
                                                        class="btn btn-icon rounded-circle btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top" title="Hapus">
                                                        <i data-feather="trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Transkrip Nilai</td>
                                                <td>Minimal Nilai IPK 3.5/4.0 </td>
                                                <td><span class="badge bg-success">Wajib</span></td>
                                                <td>
                                                    <button type="button"
                                                        class="btn btn-icon rounded-circle btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top" title="Hapus">
                                                        <i data-feather="trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>KTP</td>
                                                <td>-</td>
                                                <td><span class="badge bg-success">Wajib</span></td>
                                                <td>
                                                    <button type="button"
                                                        class="btn btn-icon rounded-circle btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top" title="Hapus">
                                                        <i data-feather="trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <h3 class="title">Pelatihan & Diklat</h3>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="datatables-basic table" id="pelatihan-diklat">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Kategori Pelatihan & Diklat</th>
                                                <th>Keterangan</th>
                                                <th>Status</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Diklat Keperawatan</td>
                                                <td>Diklat umum</td>
                                                <td><span class="badge bg-success">Wajib</span></td>
                                                <td>
                                                    <button type="button"
                                                        class="btn btn-icon rounded-circle btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top" title="Hapus">
                                                        <i data-feather="trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Pelatihan Keperawatan</td>
                                                <td>Pelatihan Keperawatan Tingkat Dasar</td>
                                                <td><span class="badge bg-success">Wajib</span></td>
                                                <td>
                                                    <button type="button"
                                                        class="btn btn-icon rounded-circle btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top" title="Hapus">
                                                        <i data-feather="trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Diklat Kepemimpinan</td>
                                                <td>Kepemimpinan Utama</td>
                                                <td><span class="badge bg-success">Wajib</span></td>
                                                <td>
                                                    <button type="button"
                                                        class="btn btn-icon rounded-circle btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top" title="Hapus">
                                                        <i data-feather="trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ general tab -->

                    <!-- change password -->
                    <div class="tab-pane fade" id="account-vertical-jadwal" role="tabpanel"
                        aria-labelledby="account-pill-jadwal" aria-expanded="false">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="title">Jadwal Kerja</h3>
                                <div class="d-flex justify-content-end mb-1">
                                    <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                        data-bs-target="#add-jadwal">
                                        <i data-feather="plus" class="me-25"></i>
                                        <span>Tambah Data</span>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="datatables-basic table" id="jadwal-kerja">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Jadwal</th>
                                                <th>Presensi Masuk</th>
                                                <th>Presensi Pulang</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Shift Pagi</td>
                                                <td>07:00 WIB</td>
                                                <td>14:00 WIB</td>
                                                <td>
                                                    <button type="button"
                                                        class="btn btn-icon rounded-circle btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top" title="Hapus">
                                                        <i data-feather="trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Shift Siang</td>
                                                <td>14:00 WIB</td>
                                                <td>20:00 WIB</td>
                                                <td>
                                                    <button type="button"
                                                        class="btn btn-icon rounded-circle btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top" title="Hapus">
                                                        <i data-feather="trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>Shift Malam</td>
                                                <td>20:00 WIB</td>
                                                <td>07:00 WIB</td>
                                                <td>
                                                    <button type="button"
                                                        class="btn btn-icon rounded-circle btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top" title="Hapus">
                                                        <i data-feather="trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ change password -->

                    <div class="tab-pane fade" id="account-vertical-tunjangan" role="tabpanel"
                        aria-labelledby="account-pill-jadwal" aria-expanded="false">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="title">Tunjangan</h3>
                                <div class="d-flex justify-content-end mb-1">
                                    <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                        data-bs-target="#add-tunjangan">
                                        <i data-feather="plus" class="me-25"></i>
                                        <span>Tambah Data</span>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="datatables-basic table" id="jadwal-kerja">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Tunjangan</th>
                                                <th>Berdasarkan Tunjangan</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Tunjangan Kesehatan</td>
                                                <td>Jabatan</td>
                                                <td>
                                                    <button type="button"
                                                        class="btn btn-icon rounded-circle btn-outline-info"
                                                        data-toggle="tooltip" data-placement="top" title="Detail"
                                                        onclick="window.location = '{{ url('/data-karyawan/detail') }}'">
                                                        <i data-feather="eye"></i>
                                                    </button>
                                                    <button type="button"
                                                        class="btn btn-icon rounded-circle btn-outline-danger"
                                                        data-toggle="tooltip" data-placement="top" title="Hapus">
                                                        <i data-feather="trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ right content section -->
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade text-start" id="add-tunjangan" tabindex="-1" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel33">Tambah Data</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="#">
                    <div class="modal-body">
                        <div class="mb-1">
                            <label class="form-label">Pilih Tunjangan</label>
                            <div class="mb-1">
                                <select class="select2 form-select w-100" id="select2-tunjangan">
                                    <option value="1">Tunjangan Kesehatan</option>
                                    <option value="2">Tunjangan Ketenagakerjaan</option>
                                    <option value="2">Tunjangan Jabatan</option>
                                </select>
                            </div>
                        </div>
                        <div class="mb-1">
                            <label class="form-label">Pilih Tunjangan Berdasarkan</label>
                            <div class="mb-1">
                                <select class="select2 form-select w-100" id="select2-tunjangan-berdasarkan">
                                    <option value="0" disabled>Pilih Tunjangan Berdasarkan</option>
                                    <option value="1">Jabatan</option>
                                    <option value="2">Profesi</option>
                                    <option value="3">Lama Kerja</option>
                                    <option value="4">Pendidikan</option>
                                </select>
                            </div>
                        </div>

                        <div class="divider">
                            <div class="divider-text">Default</div>
                        </div>

                        <section id="tunjanganberdasarkan-jabatan">
                            <div class="row">
                                @for ($i = 0; $i < 4; $i++)
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <div class="mb-1">
                                                <input type="text" value="Jabatan {{ $i }}" readonly
                                                    class="form-control"  />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="mb-1">
                                            <div class="mb-1">
                                                <input type="text" placeholder="Masukkan Nominal Tunjangan {{ $i }}"
                                                    class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                @endfor
                            </div>

                        </section>
                        <section id="tunjanganberdasarkan-jabatan">

                        </section>
                        <section id="tunjanganberdasarkan-jabatan">

                        </section>
                        <section id="tunjanganberdasarkan-jabatan">

                        </section>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade text-start" id="add-jadwal" tabindex="-1" aria-labelledby="myModalLabel1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel33">Tambah Data</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="#">
                    <div class="modal-body">
                        <div class="mb-1">
                            <label class="form-label">Nama Jadwal</label>
                            <div class="mb-1">
                                <input type="text" placeholder="Masukkan nama jadwal" class="form-control" />
                            </div>
                        </div>
                        <div class="mb-1">
                            <label class="form-label">Presensi Masuk</label>
                            <div class="mb-1">
                                <input type="time" placeholder="Masukkan presensi masuk" class="form-control" />
                            </div>
                        </div>
                        <div class="mb-1">
                            <label class="form-label">Presensi Pulang</label>
                            <div class="mb-1">
                                <input type="time" placeholder="Masukkan presensi pulang" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
