@extends('layout.main')

@section('custom-css')
    <style></style>
@endsection

@section('custom-js')
    <script>
        $(function() {
            'use strict';

            var bsStepper = document.querySelectorAll('.bs-stepper'),
                select = $('.select2'),
                horizontalWizard = document.querySelector('.horizontal-wizard-example');

            // Adds crossed class
            if (typeof bsStepper !== undefined && bsStepper !== null) {
                for (var el = 0; el < bsStepper.length; ++el) {
                    bsStepper[el].addEventListener('show.bs-stepper', function(event) {
                        var index = event.detail.indexStep;
                        var numberOfSteps = $(event.target).find('.step').length - 1;
                        var line = $(event.target).find('.step');

                        // The first for loop is for increasing the steps,
                        // the second is for turning them off when going back
                        // and the third with the if statement because the last line
                        // can't seem to turn off when I press the first item. ¯\_(ツ)_/¯

                        for (var i = 0; i < index; i++) {
                            line[i].classList.add('crossed');

                            for (var j = index; j < numberOfSteps; j++) {
                                line[j].classList.remove('crossed');
                            }
                        }
                        if (event.detail.to == 0) {
                            for (var k = index; k < numberOfSteps; k++) {
                                line[k].classList.remove('crossed');
                            }
                            line[0].classList.remove('crossed');
                        }
                    });
                }
            }

            // select2
            select.each(function() {
                var $this = $(this);
                $this.wrap('<div class="position-relative"></div>');
                $this.select2({
                    placeholder: 'Select value',
                    dropdownParent: $this.parent()
                });
            });

            // Horizontal Wizard
            // --------------------------------------------------------------------
            if (typeof horizontalWizard !== undefined && horizontalWizard !== null) {
                var numberedStepper = new Stepper(horizontalWizard),
                    $form = $(horizontalWizard).find('form');
                $form.each(function() {
                    var $this = $(this);
                    $this.validate({
                        rules: {
                            username: {
                                required: true
                            },
                            email: {
                                required: true
                            },
                            password: {
                                required: true
                            },
                            'confirm-password': {
                                required: true,
                                equalTo: '#password'
                            },
                            'first-name': {
                                required: true
                            },
                            'last-name': {
                                required: true
                            },
                            address: {
                                required: true
                            },
                            landmark: {
                                required: true
                            },
                            country: {
                                required: true
                            },
                            language: {
                                required: true
                            },
                            twitter: {
                                required: true,
                                url: true
                            },
                            facebook: {
                                required: true,
                                url: true
                            },
                            google: {
                                required: true,
                                url: true
                            },
                            linkedin: {
                                required: true,
                                url: true
                            }
                        }
                    });
                });

                $(horizontalWizard)
                    .find('.btn-next')
                    .each(function() {
                        $(this).on('click', function(e) {
                            var isValid = $(this).parent().siblings('form').valid();
                            if (isValid) {
                                numberedStepper.next();
                            } else {
                                e.preventDefault();
                            }
                        });
                    });

                $(horizontalWizard)
                    .find('.btn-prev')
                    .on('click', function() {
                        numberedStepper.previous();
                    });

                $(horizontalWizard)
                    .find('.btn-submit')
                    .on('click', function() {
                        var isValid = $(this).parent().siblings('form').valid();
                        if (isValid) {
                            alert('Submitted..!!');
                        }
                    });
            }
        })
    </script>
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">{{ $title }}</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Beranda</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Data Master</a>
                            </li>
                            <li class="breadcrumb-item active">{{ $title }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
            <div class="mb-1 breadcrumb-right">
                <button class="btn btn-outline-primary" type="button" onclick="history.back()">Kembali</button>
            </div>
        </div>
    </div>

    <div class="content-body">
        <section class="horizontal-wizard">
            <div class="bs-stepper horizontal-wizard-example">
                <div class="bs-stepper-header d-flex justify-content-center" role="tablist">
                    <div class="step" data-target="#one-step" role="tab" id="one-step-trigger">
                        <button type="button" class="step-trigger">
                            <span class="bs-stepper-box">1</span>
                            <span class="bs-stepper-label">
                                <span class="bs-stepper-title">Identitas Karyawan</span>
                                <span class="bs-stepper-subtitle">Informasi data pribadi.</span>
                            </span>
                        </button>
                    </div>
                    <div class="line px-1">
                        <i data-feather="chevron-right" class="font-medium-2"></i>
                    </div>
                    <div class="step" data-target="#two-step" role="tab" id="two-step-trigger">
                        <button type="button" class="step-trigger">
                            <span class="bs-stepper-box">2</span>
                            <span class="bs-stepper-label">
                                <span class="bs-stepper-title">Informasi Kontak</span>
                                <span class="bs-stepper-subtitle">Informasi kontak dan data keluarga.</span>
                            </span>
                        </button>
                    </div>
                    <div class="line px-1">
                        <i data-feather="chevron-right" class="font-medium-2"></i>
                    </div>
                    <div class="step" data-target="#tree-step" role="tab" id="tree-step-trigger">
                        <button type="button" class="step-trigger">
                            <span class="bs-stepper-box">3</span>
                            <span class="bs-stepper-label">
                                <span class="bs-stepper-title">Kontrak Kerja</span>
                                <span class="bs-stepper-subtitle">Informasi kontrak kerja.</span>
                            </span>
                        </button>
                    </div>
                    <div class="line px-1">
                        <i data-feather="chevron-right" class="font-medium-2"></i>
                    </div>
                    <div class="step" data-target="#four-step" role="tab" id="four-step-trigger">
                        <button type="button" class="step-trigger">
                            <span class="bs-stepper-box">4</span>
                            <span class="bs-stepper-label">
                                <span class="bs-stepper-title">Info Lainnya</span>
                                <span class="bs-stepper-subtitle">Informasi tambahan.</span>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="bs-stepper-content">
                    <div id="one-step" class="content" role="tabpanel" aria-labelledby="one-step-trigger">
                        <div class="content-header">
                            <h5 class="mb-0">Identitas Karyawan</h5>
                            <small class="text-muted">Informasi data pribadi</small>
                        </div>
                        <form id="jquery-val-form" method="post">
                            <div class="row">
                                <div class="mb-1 col-md-6">
                                    <label class="form-label">Nomor Induk Pegawai (NIP)</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control"
                                            placeholder="Masukkan Nomor Induk Pegawai (NIP)">
                                        <button class="btn btn-primary" id="button-cek" type="button">Cek</button>
                                    </div>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label">Kewarganegaraan</label>
                                    <select class="select2 form-select w-100" id="select2-kewarganegawaan" required>
                                        <option value="IS">Warga Negara Indonesia (WNI)</option>
                                        <option value="KP">Warga Negara Asing (WNA)</option>
                                    </select>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label">Nama Lengkap</label>
                                    <input type="text" class="form-control" placeholder="Masukkan Nama Lengkap"
                                        required />
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label">Pendidikan Terakhir</label>
                                    <select class="select2 form-select w-100" id="select2-pendidikan" required>
                                        <option value="IS">SD</option>
                                        <option value="KP">SMP/SLTP</option>
                                        <option value="KK">SMA/SLTA/SMK</option>
                                        <option value="BH">Diploma 3 (D3)</option>
                                        <option value="BH">Diploma 4 (D4)</option>
                                        <option value="HI">Sarjana 1 (S1)</option>
                                        <option value="HI">Sarjana 2 (S2)</option>
                                        <option value="HI">Sarjana 3 (S3)</option>
                                    </select>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label">Jenis Kelamin</label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input1" type="radio" name="inlineRadioOptions"
                                                    id="inlineRadio4" value="option4" checked="">
                                                <label class="form-check-label" for="inlineRadio4">Laki - Laki</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input1" type="radio" name="inlineRadioOptions"
                                                    id="inlineRadio5" value="option5" checked="">
                                                <label class="form-check-label" for="inlineRadio5">Perempuan</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label">Status Pernikahan</label>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                                    id="inlineRadio1" value="option1" checked="">
                                                <label class="form-check-label" for="inlineRadio1">Belum Menikah</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                                    id="inlineRadio2" value="option2" checked="">
                                                <label class="form-check-label" for="inlineRadio2">Menikah</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions"
                                                    id="inlineRadio3" value="option3" checked="">
                                                <label class="form-check-label" for="inlineRadio3">Cerai</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label">Tempat, Tanggal lahir</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" placeholder="Masukkan tempat lahir"
                                                required />
                                        </div>
                                        <div class="col-md-6">
                                            <input type="date" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label">Nomor Induk Kependudukan (NIK)</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="number" class="form-control"
                                                placeholder="Masukkan Nomor Induk Kependudukan (NIK)" required />
                                        </div>
                                        <div class="col-md-6">
                                            <input type="file" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label">Agama</label>
                                    <select class="select2 form-select w-100" id="select2-agama" required>
                                        <option value="IS">Islam</option>
                                        <option value="KP">Kristen Protestan</option>
                                        <option value="KK">Kristen Katholik</option>
                                        <option value="BH">Budha</option>
                                        <option value="HI">Hindu</option>
                                        <option value="TI">Tionghoa</option>
                                    </select>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label">NPWP</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="number" class="form-control" placeholder="Masukkan NPWP"
                                                required />
                                        </div>
                                        <div class="col-md-6">
                                            <input type="file" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label">Golongan Darah</label>
                                    <select class="select2 form-select w-100" id="select2-gol-darah" required>
                                        <option value="A">Golongan Darah A</option>
                                        <option value="B">Golongan Darah B</option>
                                        <option value="AB">Golongan Darah ABk</option>
                                        <option value="O">Golongan Darah O</option>
                                    </select>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label">Foto Karyawan</label>
                                    <input type="file" class="form-control" required />
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label">BPJS Kesehatan</label>
                                    <input type="text" class="form-control" placeholder="Masukkan BPJS Kesehatan"
                                        required />
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label">BPJS Kewarganegaraan</label>
                                    <input type="text" class="form-control" placeholder="Masukkan BPJS Kewarganegaraan"
                                        required />
                                </div>
                            </div>
                        </form>
                        <div class="d-flex justify-content-between">
                            <button class="btn btn-outline-secondary btn-prev" disabled>
                                <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                <span class="align-middle d-sm-inline-block d-none">Previous</span>
                            </button>
                            <button class="btn btn-primary btn-next">
                                <span class="align-middle d-sm-inline-block d-none">Next</span>
                                <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                            </button>
                        </div>
                    </div>
                    <div id="two-step" class="content" role="tabpanel" aria-labelledby="two-step-trigger">
                        <div class="content-header">
                            <h5 class="mb-0">Informasi Kontak</h5>
                            <small>Informasi kontak pribadi.</small>
                        </div>

                        <div class="row">
                            <div class="mb-1 col-md-6">
                                <label class="form-label">Nomor Handphone</label>
                                <input type="text" class="form-control" placeholder="Masukkan nomor handphone"
                                    required />
                            </div>
                            <div class="mb-1 col-md-6">
                                <label class="form-label">Email</label>
                                <input type="email" class="form-control" placeholder="Masukkan email" required />
                            </div>

                            <div class="mb-1 col-md-12">
                                <label class="form-label">Alamat Domisili</label>
                                <textarea class="form-control" rows="2"
                                    placeholder="Masukkan alamat domisili"></textarea>
                            </div>
                        </div>

                        <div class="content-header mt-3">
                            <h5 class="mb-0">Data Keluarga</h5>
                            <small>Informasi Data keluarga.</small>
                        </div>
                        <form>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-2">
                                        <label class="form-label">Nama Bapak</label>
                                        <input type="text" class="form-control" placeholder="Masukkan nama bapak"
                                            required />
                                    </div>
                                    <div class="mb-2">
                                        <label class="form-label">Nama Ibu</label>
                                        <input type="text" class="form-control" placeholder="Masukkan nama ibu"
                                            required />
                                    </div>
                                    <div class="mb-2">
                                        <label class="form-label">Nama Suami/Istri</label>
                                        <input type="text" class="form-control" placeholder="Masukkan nama suami/istri"
                                            required />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-2">
                                        <label class="form-label">Kontak Keluarga yang dapat
                                            dihubungi</label>
                                        <div class="mb-1">
                                            <select class="select2 form-select" id="select2-kontak-keluarga" required>
                                                <option value="BP">Bapak</option>
                                                <option value="IB">Ibu</option>
                                                <option value="SK">Saudara Kandung</option>
                                                <option value="WL">Wali</option>
                                                <option value="SI">Suami/Istri</option>
                                            </select>
                                        </div>
                                        <input type="number" class="form-control" placeholder="Masukkan kontak keluarga"
                                            required />
                                    </div>
                                    <div class="mb-2">
                                        <label class="form-label">Alamat Keluarga Yang Dapat Dihubungi </label>
                                        <textarea class="form-control" rows="2"
                                            placeholder="Masukkan Alamat Keluarga Yang Dapat Dihubungi	"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="divider">
                                <div class="divider-text">Data Anak</div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex justify-content-end">
                                        <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                            data-bs-target="#add-anak">
                                            <i data-feather="plus" class="me-25"></i>
                                            <span>Tambah Data</span>
                                        </button>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="datatables-basic table">
                                            <thead>
                                                <tr>
                                                    <th>Anak ke-</th>
                                                    <th>Nama Anak</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <!-- Modal -->
                        <div class="modal fade text-start" id="add-anak" tabindex="-1" aria-labelledby="myModalLabel1"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel33">Tambah Data</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form action="#">
                                        <div class="modal-body">
                                            <label class="form-label">Nama Anak Ke-</label>
                                            <div class="mb-1">
                                                <input type="text" placeholder="Masukkan nama unit"
                                                    class="form-control" />
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-secondary"
                                                data-bs-dismiss="modal">Batal</button>
                                            <button type="button" class="btn btn-primary"
                                                data-bs-dismiss="modal">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="d-flex justify-content-between">
                            <button class="btn btn-primary btn-prev">
                                <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                <span class="align-middle d-sm-inline-block d-none">Previous</span>
                            </button>
                            <button class="btn btn-primary btn-next">
                                <span class="align-middle d-sm-inline-block d-none">Next</span>
                                <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                            </button>
                        </div>
                    </div>
                    <div id="tree-step" class="content" role="tabpanel" aria-labelledby="tree-step-trigger">
                        <div class="content-header">
                            <h5 class="mb-0">Kontrak Kerja</h5>
                            <small>Posisi pekerjaan.</small>
                        </div>

                        <div class="divider">
                            <div class="divider-text">Kontrak Kerja</div>
                        </div>

                        <form>
                            <div class="row">
                                <div class="mb-1 col-md-12">
                                    <label class="form-label">Nomor Kontrak</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Masukkan Nomor Kontrak">
                                        <button class="btn btn-primary" id="button-cek" type="button">Cek</button>
                                    </div>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label" for="kontrak masuk">Kontrak Masuk</label>
                                    <input type="date" class="form-control" placeholder="Pilih kontrak masuk" required />
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label" for="kontrak selesai">Kontrak Selesai</label>
                                    <input type="date" class="form-control" placeholder="Pilih kontrak selesai"
                                        required />
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label" for="Unit">Unit</label>
                                    <select class="select2 form-select w-100" id="select2-unit" required>
                                        <option value="BP">Unit 1</option>
                                        <option value="IB">Unit 2</option>
                                        <option value="SK">Unit 3</option>
                                    </select>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label" for="Jabatan">Jabatan</label>
                                    <select class="select2 form-select w-100" id="select2-jabatan" required>
                                        <option value="BP">Jabatan 1</option>
                                        <option value="IB">Jabatan 2</option>
                                        <option value="SK">Jabatan 3</option>
                                    </select>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label" for="Profesi">Profesi</label>
                                    <select class="select2 form-select w-100" id="select2-profesi" required>
                                        <option value="BP">Profesi 1</option>
                                        <option value="IB">Profesi 2</option>
                                        <option value="SK">Profesi 3</option>
                                    </select>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label" for="Gaji Pokok">Gaji Pokok</label>
                                    <input type="number" class="form-control" placeholder="Masukkan gaji pokok"
                                        required />
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label" for="Nama Bank">Nama Bank</label>
                                    <input type="number" class="form-control" placeholder="Masukkan nama bank"
                                        required />
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label" for="Gaji Pokok">Nomor Rekening</label>
                                    <input type="number" class="form-control" placeholder="Masukkan monor rekening"
                                        required />
                                </div>
                            </div>

                            <div class="divider">
                                <div class="divider-text">Tunjangan</div>
                            </div>

                            @for ($i = 0; $i < 4; $i++)
                                <div class="row">
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="Profesi">Nama Tunjangan</label>
                                        <input type="text" class="form-control" value="Tunjangan Kesehatan" disabled />
                                    </div>
                                    <div class="mb-1 col-md-6">
                                        <label class="form-label" for="Nominal">Nominal</label>
                                        <input type="number" class="form-control" placeholder="Masukkan nominal"
                                            value="0" required />
                                    </div>
                                </div>
                            @endfor

                            <div class="row">
                                <div class="mb-1 col-md-12">
                                    <label class="form-label" for="Uploud">Dokumen Kontrak</label>
                                    <input type="file" class="form-control" placeholder="Uploud Dokumen" required />
                                </div>
                            </div>
                        </form>

                        <div class="d-flex justify-content-between">
                            <button class="btn btn-primary btn-prev">
                                <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                <span class="align-middle d-sm-inline-block d-none">Previous</span>
                            </button>
                            <button class="btn btn-primary btn-next">
                                <span class="align-middle d-sm-inline-block d-none">Next</span>
                                <i data-feather="arrow-right" class="align-middle ms-sm-25 ms-0"></i>
                            </button>
                        </div>
                    </div>
                    <div id="four-step" class="content" role="tabpanel" aria-labelledby="four-step-trigger">
                        <div class="content-header">
                            <h5 class="mb-0">Riwayat Pendidikan</h5>
                            <small>Informasi tambahan pendidikan.</small>
                        </div>
                        <form>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex justify-content-end">
                                        <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                            data-bs-target="#add-pendidikan">
                                            <i data-feather="plus" class="me-25"></i>
                                            <span>Tambah Data</span>
                                        </button>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="datatables-basic table" id="table-diklat">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Pendidikan Formal</th>
                                                    <th>Instansi Pendidikan</th>
                                                    <th>Tahun Masuk</th>
                                                    <th>Tahun Lulus</th>
                                                    <th>IPK</th>
                                                    <th>Status</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>D4 Keperawatan</td>
                                                    <td>Politeknik Kesehatan Kemenkes Bandung</td>
                                                    <td>2014</td>
                                                    <td>2018</td>
                                                    <td>3.60</td>
                                                    <td><span class="badge bg-danger text-uppercase">Belum
                                                            Terverifikasi</span></td>
                                                    <td>
                                                        <button type="button"
                                                            class="btn btn-icon rounded-circle btn-outline-primary"
                                                            data-toggle="tooltip" data-placement="top"
                                                            title="Download Dokumen">
                                                            <i data-feather="download"></i>
                                                        </button>
                                                        <button type="button"
                                                            class="btn btn-icon rounded-circle btn-outline-danger"
                                                            data-toggle="tooltip" data-placement="top" title="Hapus">
                                                            <i data-feather="trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="content-header mt-3">
                                <h5 class="mb-0">Pelatihan & Diklat</h5>
                                <small>Informasi tambahan pelatihan & diklat Karyawan.</small>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="d-flex justify-content-end">
                                        <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                            data-bs-target="#add-diklat">
                                            <i data-feather="plus" class="me-25"></i>
                                            <span>Tambah Data</span>
                                        </button>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="datatables-basic table" id="table-diklat">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Kategori Pelatihan/Diklat</th>
                                                    <th>Nama Pelatihan/Diklat</th>
                                                    <th>Tahun Pelatihan/Diklat</th>
                                                    <th>Institusi Penyelenggara</th>
                                                    <th>Download Dokumen</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Pelatihan Keperawatan Nasional</td>
                                                    <td>Pelatihan Keperawatan Sejawabarat</td>
                                                    <td>2017</td>
                                                    <td>Kementerian Kesehatan Bandung</td>
                                                    <td>
                                                        <button type="button"
                                                            class="btn btn-icon rounded-circle btn-outline-primary"
                                                            data-toggle="tooltip" data-placement="top"
                                                            title="Download Dokumen">
                                                            <i data-feather="download"></i>
                                                        </button>
                                                        <button type="button"
                                                            class="btn btn-icon rounded-circle btn-outline-danger"
                                                            data-toggle="tooltip" data-placement="top" title="Hapus">
                                                            <i data-feather="trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="content-header mt-3">
                                <h5 class="mb-0">Dokumen Legalitas</h5>
                                <small>Informasi tambahan pelatihan & diklat Karyawan.</small>
                            </div>

                            <div class="row">
                                <div class="mb-1 col-md-12">
                                    <div class="d-flex justify-content-end">
                                        <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                            data-bs-target="#add-dokumen">
                                            <i data-feather="plus" class="me-25"></i>
                                            <span>Tambah Data</span>
                                        </button>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="datatables-basic table" id="dokumen-legalitas">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Dokumen</th>
                                                    <th>Masa Berlaku</th>
                                                    <th>Status</th>
                                                    <th>Download Dokumen</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>STR Keperawatan</td>
                                                    <td>31 Desember 2025</td>
                                                    <td><span class="badge bg-danger text-uppercase">Belum
                                                            Terverifikasi</span></td>
                                                    <td>
                                                        <button type="button"
                                                            class="btn btn-icon rounded-circle btn-outline-primary"
                                                            data-toggle="tooltip" data-placement="top"
                                                            title="Download Dokumen">
                                                            <i data-feather="download"></i>
                                                        </button>
                                                        <button type="button"
                                                            class="btn btn-icon rounded-circle btn-outline-danger"
                                                            data-toggle="tooltip" data-placement="top" title="Hapus">
                                                            <i data-feather="trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <!-- Modal -->
                        <div class="modal fade text-start" id="add-dokumen" tabindex="-1" aria-labelledby="myModalLabel1"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel33">Tambah Data</h4>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form action="#">
                                        <div class="modal-body">
                                            <label class="form-label">Pilih Dokumen</label>
                                            <div class="mb-1">
                                                <select class="select2 form-select w-100" id="select2-dokumen" required>
                                                    <option value="BP">KTP</option>
                                                    <option value="IB">NPWP</option>
                                                    <option value="SK">KTR</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-secondary"
                                                data-bs-dismiss="modal">Batal</button>
                                            <button type="button" class="btn btn-primary"
                                                data-bs-dismiss="modal">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <button class="btn btn-primary btn-prev">
                                <i data-feather="arrow-left" class="align-middle me-sm-25 me-0"></i>
                                <span class="align-middle d-sm-inline-block d-none">Previous</span>
                            </button>
                            <button class="btn btn-success btn-submit">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
