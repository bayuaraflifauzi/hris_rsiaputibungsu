@extends('layout.main')

@section('custom-css')
    <style>
    </style>
@endsection

@section('custom-js')
    <script>
        $(function() {
            'use strict';
            var confirmApprove = $('#confirm-approve');
            var confirmReject = $('#confirm-reject');

            confirmApprove.on('click', function() {
                Swal.fire({
                    title: 'Apakah Anda yakin menyetujui dokumen ini?',
                    text: "",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Ya!',
                    customClass: {
                        confirmButton: 'btn btn-primary',
                        cancelButton: 'btn btn-outline-danger ms-1'
                    },
                    buttonsStyling: false,
                }).then(function(result) {
                    if (result.value) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Dokumen telah Tervalidasi!',
                            text: 'Anda telah memvalidasi dokumen tersebut.',
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        });
                    }
                });
            });

            confirmReject.on('click', function() {
                Swal.fire({
                    title: 'Apakah Anda yakin menolak dokumen ini?',
                    text: "",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Ya!',
                    customClass: {
                        confirmButton: 'btn btn-primary',
                        cancelButton: 'btn btn-outline-danger ms-1'
                    },
                    buttonsStyling: false
                }).then(function(result) {
                    if (result.value) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Dokumen ini telah ditolak!',
                            text: 'Anda telah memvalidasi dokumen tersebut.',
                            customClass: {
                                confirmButton: 'btn btn-success'
                            }
                        });
                    }
                });
            });
        })
    </script>
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">{{ $title }}</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Beranda</a>
                            </li>
                            <li class="breadcrumb-item active">{{ $title }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
            <div class="mb-1 breadcrumb-right">
                <button type="button" class="btn btn-primary">
                    <i data-feather="edit-2" class="me-25"></i>
                    <span>Edit</span>
                </button>
                <button type="button" class="btn btn-outline-primary" onclick="history.back()">
                    <span>Kembali</span>
                </button>
            </div>
        </div>
    </div>

    <div class="content-body">
        <div id="user-profile">
            <!-- profile header -->
            <div class="row">
                <div class="col-12">
                    <div class="card profile-header mb-2">
                        <!-- profile cover photo -->
                        <img class="card-img-top"
                            src="{{ asset('app-assets/images/profile/user-uploads/bg-profile1.jpeg') }}"
                            alt="User Profile Image" />
                        <!--/ profile cover photo -->

                        <div class="position-relative">
                            <!-- profile picture -->
                            <div class="profile-img-container d-flex align-items-center">
                                <div class="profile-img">
                                    <img src="{{ asset('app-assets/images/portrait/small/avatar-s-2.jpg') }}"
                                        class="rounded img-fluid" alt="Card image" />
                                </div>
                                <!-- profile title -->
                                <div class="profile-title ms-3">
                                    <h2 class="text-white">Desi Oktarika</h2>
                                    <p class="text-white"><span class="badge bg-primary text-uppercase">Pelaksana</span>
                                        | <span class="badge bg-dark text-uppercase">Unit Rawat Inap</span></p>
                                </div>
                            </div>
                        </div>

                        <!-- tabs pill -->
                        <div class="profile-header-nav">
                            <!-- navbar -->
                            <nav
                                class="navbar navbar-expand-md navbar-light justify-content-end justify-content-md-between w-100">
                                <button class="btn btn-icon navbar-toggler" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                    aria-expanded="false" aria-label="Toggle navigation">
                                    <i data-feather="align-justify" class="font-medium-5"></i>
                                </button>

                                <!-- collapse  -->
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <div class="profile-tabs d-flex justify-content-between flex-wrap mt-1 mt-md-0">
                                        <ul class="nav nav-pills mb-0">
                                            <li class="nav-item">
                                                <a class="nav-link fw-bold active" id="tab-data-pribadi-pill"
                                                    data-bs-toggle="pill" href="#tab-data-pribadi">
                                                    <span class="d-none d-md-block">Data Pribadi</span>
                                                    <i data-feather="rss" class="d-block d-md-none"></i>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link fw-bold" id="tab-riwayat-penempatan-pill"
                                                    data-bs-toggle="pill" href="#tab-riwayat-penempatan">
                                                    <span class="d-none d-md-block">Riwayat Penempatan</span>
                                                    <i data-feather="info" class="d-block d-md-none"></i>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link fw-bold" id="tab-dokumen-legalitas-pill"
                                                    data-bs-toggle="pill" href="#tab-dokumen-legalitas">
                                                    <span class="d-none d-md-block">Dokumen Legalitas</span>
                                                    <span
                                                        class="badge badge-light-danger rounded-pill ms-auto">1</span></span>
                                                    <i data-feather="image" class="d-block d-md-none"></i>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link fw-bold" id="tab-pendidikan-diklat-pill"
                                                    data-bs-toggle="pill" href="#tab-pendidikan-diklat">
                                                    <span class="d-none d-md-block">Pendidikan & Diklat</span>
                                                    <span class="badge badge-light-danger rounded-pill ms-auto">1</span>
                                                    <i data-feather="image" class="d-block d-md-none"></i>
                                                </a>
                                            </li>
                                            <li class="nav-item dropdown">
                                                <a class="nav-link fw-bold dropdown-toggle" data-bs-toggle="dropdown"
                                                    href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                                    <span class="d-none d-md-block">Informasi Lainnya</span>
                                                </a>
                                                <div class="dropdown-menu">
                                                    <a class="dropdown-item" id="dropdown1-tab" href="#tab-penilaian"
                                                        data-bs-toggle="pill" aria-expanded="true">Penilaian</a>
                                                    <a class="dropdown-item" id="dropdown2-tab" href="#tab-penilaian"
                                                        data-bs-toggle="pill" aria-expanded="true">Informasi Lainnya</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!--/ collapse  -->
                            </nav>
                            <!--/ navbar -->
                        </div>
                    </div>
                </div>
            </div>
            <!--/ profile header -->

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab-data-pribadi" aria-expanded="true">
                    <!-- profile info section -->
                    <section id="profile-info">
                        <div class="row">
                            <!-- left profile info section -->
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header border">
                                        <h2 class="title mb-0 text-uppercase">Profesi & Jabatan Terkini</h2>
                                    </div>
                                    <div class="card-body">
                                        <div class="row mt-2">
                                            <div class="mb-1 col-md-4">
                                                <label class="form-label">Unit</label>
                                                <input type="text" class="form-control"
                                                    value="Unit Rawat Inap" readonly />
                                            </div>
                                            <div class="mb-1 col-md-4">
                                                <label class="form-label">Jabatan</label>
                                                <input type="text" class="form-control"
                                                    value="Pelaksana" readonly />
                                            </div>
                                            <div class="mb-1 col-md-4">
                                                <label class="form-label">Profesi</label>
                                                <input type="text" class="form-control"
                                                    value="Perawat" readonly />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <!-- about -->
                                <div class="card">
                                    <div class="card-header border">
                                        <h2 class="title mb-0 text-uppercase">Identitas Karyawan</h2>
                                    </div>
                                    <div class="card-body">
                                        <div class="row mt-2">
                                            <div class="mb-1 col-md-6">
                                                <label class="form-label">Nomor Induk Pegawai (NIP)</label>
                                                <input type="text" class="form-control" value="1234567890" readonly>
                                            </div>
                                            <div class="mb-1 col-md-6">
                                                <label class="form-label">Kewarganegaraan</label>
                                                <select class="select2 form-select w-100" id="select2-kewarganegawaan"
                                                    disabled>
                                                    <option value="IS">Warga Negara Indonesia (WNI)</option>
                                                    <option value="KP">Warga Negara Asing (WNA)</option>
                                                </select>
                                            </div>
                                            <div class="mb-1 col-md-6">
                                                <label class="form-label">Nama Lengkap</label>
                                                <input type="text" class="form-control" value="Desi Oktarika" readonly />
                                            </div>
                                            <div class="mb-1 col-md-6">
                                                <label class="form-label">Pendidikan Terakhir</label>
                                                <select class="select2 form-select w-100" id="select2-pendidikan" disabled>
                                                    <option value="IS">SD</option>
                                                    <option value="KP">SMP/SLTP</option>
                                                    <option value="KK">SMA/SLTA/SMK</option>
                                                    <option value="BH">Diploma 3 (D3)</option>
                                                    <option value="BH">Diploma 4 (D4)</option>
                                                    <option value="HI" selected>Sarjana 1 (S1)</option>
                                                    <option value="HI">Sarjana 2 (S2)</option>
                                                    <option value="HI">Sarjana 3 (S3)</option>
                                                </select>
                                            </div>
                                            <div class="mb-1 col-md-6">
                                                <label class="form-label">Jenis Kelamin</label>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-check form-check-inline p-0">
                                                            <input class="form-check-input1" type="radio"
                                                                name="inlineRadioOptions" id="inlineRadio4" value="option4"
                                                                disabled>
                                                            <label class="form-check-label" for="inlineRadio4">Laki -
                                                                Laki</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input1" type="radio"
                                                                name="inlineRadioOptions" id="inlineRadio5" value="option5"
                                                                checked="true" disabled>
                                                            <label class="form-check-label"
                                                                for="inlineRadio5">Perempuan</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-1 col-md-6">
                                                <label class="form-label">Status Pernikahan</label>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio"
                                                                name="inlineRadioOptions" id="inlineRadio1" value="option1"
                                                                checked="true" disabled>
                                                            <label class="form-check-label" for="inlineRadio1">Belum
                                                                Menikah</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio"
                                                                name="inlineRadioOptions" id="inlineRadio2" value="option2"
                                                                disabled>
                                                            <label class="form-check-label"
                                                                for="inlineRadio2">Menikah</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio"
                                                                name="inlineRadioOptions" id="inlineRadio3" value="option3"
                                                                disabled>
                                                            <label class="form-check-label"
                                                                for="inlineRadio3">Cerai</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-1 col-md-6">
                                                <label class="form-label">Tempat, Tanggal lahir</label>
                                                <div class="row">
                                                    <div class="col-md-6 mb-1">
                                                        <input type="text" class="form-control" value="Palembang"
                                                            readonly />
                                                    </div>
                                                    <div class="col-md-6 mb-1">
                                                        <input type="date" class="form-control" readonly />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-1 col-md-6">
                                                <label class="form-label">Nomor Induk Kependudukan (NIK)</label>
                                                <div class="row">
                                                    <div class="col-md-10 mb-1">
                                                        <input type="number" class="form-control" value="1234567890"
                                                            readonly />
                                                    </div>
                                                    <div class="col-md-1 mb-1">
                                                        <button type="button"
                                                            class="btn btn-icon rounded-circle btn-outline-info"
                                                            data-toggle="tooltip" data-placement="top" title="Download">
                                                            <i data-feather="download"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-1 col-md-6">
                                                <label class="form-label">Agama</label>
                                                <select class="select2 form-select w-100" id="select2-agama" disabled>
                                                    <option value="IS">Islam</option>
                                                    <option value="KP">Kristen Protestan</option>
                                                    <option value="KK">Kristen Katholik</option>
                                                    <option value="BH">Budha</option>
                                                    <option value="HI">Hindu</option>
                                                    <option value="TI">Tionghoa</option>
                                                </select>
                                            </div>
                                            <div class="mb-1 col-md-6">
                                                <label class="form-label">NPWP</label>
                                                <div class="row">
                                                    <div class="col-md-10 mb-1">
                                                        <input type="number" class="form-control" value="1234567890"
                                                            readonly />
                                                    </div>
                                                    <div class="col-md-1 mb-1">
                                                        <button type="button"
                                                            class="btn btn-icon rounded-circle btn-outline-info"
                                                            data-toggle="tooltip" data-placement="top" title="Download">
                                                            <i data-feather="download"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-1 col-md-6">
                                                <label class="form-label">Golongan Darah</label>
                                                <select class="select2 form-select w-100" id="select2-gol-darah" disabled>
                                                    <option value="A">Golongan Darah A</option>
                                                    <option value="B">Golongan Darah B</option>
                                                    <option value="AB">Golongan Darah ABk</option>
                                                    <option value="O">Golongan Darah O</option>
                                                </select>
                                            </div>
                                            <div class="mb-1 col-md-6">
                                                <label class="form-label">BPJS Kewarganegaraan</label>
                                                <input type="text" class="form-control" value="1234567890" readonly />
                                            </div>
                                            <div class="mb-1 col-md-6">
                                                <label class="form-label">BPJS Kesehatan</label>
                                                <input type="text" class="form-control" value="1234567890" readonly />
                                            </div>
                                            <div class="mb-1 col-md-6">
                                                <label class="form-label">Rekening Bank</label>
                                                <div class="row">
                                                    <div class="col-md-6 mb-1">
                                                        <input type="text" class="form-control" value="Bank Mandiri"
                                                            readonly />
                                                    </div>
                                                    <div class="col-md-6 mb-1">
                                                        <input type="text" class="form-control" value="123517623"
                                                            readonly />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ about -->
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header border">
                                        <h2 class="title mb-0 text-uppercase">Kontak Karyawan</h2>
                                    </div>
                                    <div class="card-body">
                                        <div class="row mt-2">
                                            <div class="mb-1 col-md-6">
                                                <label class="form-label">Nomor Handphone</label>
                                                <input type="text" class="form-control"
                                                    value="1234567890" readonly />
                                            </div>
                                            <div class="mb-1 col-md-6">
                                                <label class="form-label">Email</label>
                                                <input type="email" class="form-control" value="desy@gmail.com" readonly />
                                            </div>
                                            <div class="mb-1 col-md-12">
                                                <label class="form-label">Alamat Domisili</label>
                                                <textarea class="form-control" rows="2" readonly>Jalan Palembang</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header border">
                                        <h2 class="title mb-0 text-uppercase">Data Keluarga</h2>
                                    </div>
                                    <div class="card-body">
                                        <div class="row mt-2">
                                            <div class="col-md-6">
                                                <div class="mb-2">
                                                    <label class="form-label">Nama Bapak</label>
                                                    <input type="text" class="form-control" value="Fajarudin" readonly />
                                                </div>
                                                <div class="mb-2">
                                                    <label class="form-label">Nama Ibu</label>
                                                    <input type="text" class="form-control" value="Armanila" readonly />
                                                </div>
                                                <div class="mb-2">
                                                    <label class="form-label">Nama Suami/Istri</label>
                                                    <input type="text" class="form-control" value="Bayu Arafli Fauzi"
                                                        readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-2">
                                                    <label class="form-label">Kontak Keluarga yang dapat
                                                        dihubungi</label>
                                                    <div class="mb-1">
                                                        <select class="select2 form-select" id="select2-kontak-keluarga"
                                                            disabled>
                                                            <option value="BP">Bapak</option>
                                                            <option value="IB">Ibu</option>
                                                            <option value="SK">Saudara Kandung</option>
                                                            <option value="WL">Wali</option>
                                                            <option value="SI">Suami/Istri</option>
                                                        </select>
                                                    </div>
                                                    <input type="number" class="form-control" value="1234567890"
                                                        readonly />
                                                </div>
                                                <div class="mb-2">
                                                    <label class="form-label">Alamat Keluarga Yang Dapat Dihubungi
                                                    </label>
                                                    <textarea class="form-control" rows="2"
                                                        readonly>Jalan Palembang</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="divider">
                                            <div class="divider-text">Data Anak</div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="d-flex justify-content-end fade">
                                                    <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                                        data-bs-target="#add-anak">
                                                        <i data-feather="plus" class="me-25"></i>
                                                        <span>Tambah Data</span>
                                                    </button>
                                                </div>
                                                <div class="table-responsive">
                                                    <table class="datatables-basic table">
                                                        <thead>
                                                            <tr>
                                                                <th>Anak ke-</th>
                                                                <th>Nama Anak</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ left profile info section -->
                        </div>
                    </section>
                    <!--/ profile info section -->
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab-riwayat-penempatan" aria-expanded="true">
                    <section id="riwayat-penempatan">
                        <div class="row">
                            <!-- left profile info section -->
                            <div class="col-12">
                                <!-- about -->
                                <div class="card">
                                    <div class="card-header border">
                                        <h2 class="title mb-0">Riwayat Kontrak</h2>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="datatables-basic table" id="riwayat-kontrak">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Kontrak Masuk</th>
                                                        <th>Kontrak Selesai</th>
                                                        <th>Profesi</th>
                                                        <th>Status</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>1 Januari 2022</td>
                                                        <td>31 Desember 2022</td>
                                                        <td>Perawat</td>
                                                        <td><span class="badge bg-success text-uppercase">Kontrak</span>
                                                        </td>
                                                        <td>
                                                            <button type="button"
                                                                class="btn btn-icon rounded-circle btn-outline-primary"
                                                                data-toggle="tooltip" data-placement="top"
                                                                title="Download Dokumen Kontrak">
                                                                <i data-feather="download"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--/ about -->
                            </div>

                            <div class="col-12">
                                <!-- about -->
                                <div class="card">
                                    <div class="card-header border">
                                        <h2 class="title mb-0">Riwayat Penempatan</h2>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="datatables-basic table" id="riwayat-penempatan">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Tahun</th>
                                                        <th>Jabatan Lama</th>
                                                        <th>Unit Jabatan Lama</th>
                                                        <th>Jabatan Baru</th>
                                                        <th>Unit Jabatan Baru</th>
                                                        <th>File SK</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>2022</td>
                                                        <td>Pelaksana</td>
                                                        <td>Unit Rekam Medis</td>
                                                        <td>Pelaksana</td>
                                                        <td>Unit Rawat Inap</td>
                                                        <td>
                                                            <button type="button"
                                                                class="btn btn-icon rounded-circle btn-outline-primary"
                                                                data-toggle="tooltip" data-placement="top"
                                                                title="Download Dokumen SK">
                                                                <i data-feather="download"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--/ about -->
                            </div>
                            <!--/ left profile info section -->
                        </div>
                    </section>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab-dokumen-legalitas" aria-expanded="true">
                    <section id="dokumen-legalitas">
                        <div class="row">
                            <!-- left profile info section -->
                            <div class="col-md-12">
                                <!-- about -->
                                <div class="card">
                                    <div class="card-header border">
                                        <h2 class="title mb-0">Dokumen Legalitas</h2>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="datatables-basic table" id="dokumen-legalitas">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama Dokumen</th>
                                                        <th>Masa Berlaku</th>
                                                        <th>Status</th>
                                                        <th>Download Dokumen</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>STR Keperawatan</td>
                                                        <td>31 Desember 2025</td>
                                                        <td><span class="badge bg-danger text-uppercase">Belum
                                                                Terverifikasi</span></td>
                                                        <td>
                                                            <button type="button"
                                                                class="btn btn-icon rounded-circle btn-outline-primary"
                                                                data-toggle="tooltip" data-placement="top"
                                                                title="Download Dokumen">
                                                                <i data-feather="download"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--/ about -->
                            </div>
                            <!--/ left profile info section -->
                        </div>
                    </section>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab-pendidikan-diklat" aria-expanded="true">
                    <section id="pendidikan-diklat">
                        <div class="row">
                            <!-- left profile info section -->
                            <div class="col-md-12">
                                <!-- about -->
                                <div class="card">
                                    <div class="card-header border">
                                        <h2 class="title mb-0">Pelatihan & Diklat</h2>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="datatables-basic table" id="table-diklat">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Kategori Pelatihan/Diklat</th>
                                                        <th>Nama Pelatihan/Diklat</th>
                                                        <th>Tahun Pelatihan/Diklat</th>
                                                        <th>Institusi Penyelenggara</th>
                                                        <th>Download Dokumen</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Pelatihan Keperawatan Nasional</td>
                                                        <td>Pelatihan Keperawatan Sejawabarat</td>
                                                        <td>2017</td>
                                                        <td>Kementerian Kesehatan Bandung</td>
                                                        <td>
                                                            <button type="button"
                                                                class="btn btn-icon rounded-circle btn-outline-primary"
                                                                data-toggle="tooltip" data-placement="top"
                                                                title="Download Dokumen">
                                                                <i data-feather="download"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--/ about -->
                            </div>

                            <div class="col-md-12">
                                <!-- about -->
                                <div class="card">
                                    <div class="card-header border">
                                        <h2 class="title mb-0">Pendidikan</h2>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="datatables-basic table" id="table-diklat">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Pendidikan Formal</th>
                                                        <th>Instansi Pendidikan</th>
                                                        <th>Tahun Masuk</th>
                                                        <th>Tahun Lulus</th>
                                                        <th>IPK</th>
                                                        <th>Status</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>D4 Keperawatan</td>
                                                        <td>Politeknik Kesehatan Kemenkes Bandung</td>
                                                        <td>2014</td>
                                                        <td>2018</td>
                                                        <td>3.60</td>
                                                        <td><span class="badge bg-danger text-uppercase">Belum
                                                                Terverifikasi</span></td>
                                                        <td>
                                                            <button type="button"
                                                                class="btn btn-icon rounded-circle btn-outline-primary"
                                                                data-toggle="tooltip" data-placement="top"
                                                                title="Download Dokumen">
                                                                <i data-feather="download"></i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--/ about -->
                            </div>
                            <!--/ left profile info section -->
                        </div>
                    </section>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="tab-penilaian" aria-expanded="true">
                    <section id="penilaian">
                        <div class="row">
                            <!-- left profile info section -->
                            <div class="col-12">
                                <!-- about -->
                                <div class="card">
                                    <div class="card-header border">
                                        <h2 class="title mb-0">Penilaian</h2>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="mt-2">
                                                    <label class="form-label">Nama Lengkap</label>
                                                    <input type="text" class="form-control" value="RSIA Puti Bungsu"
                                                        disabled />
                                                </div>
                                                <div class="mt-2">
                                                    <label class="form-label">Nomor Induk Kependudukan (NIK)</label>
                                                    <input type="text" class="form-control" value="1234567890"
                                                        disabled />
                                                </div>
                                                <div class="mt-2">
                                                    <label class="form-label">Nomor Induk Pegawai (NIP)</label>
                                                    <input type="text" class="form-control" value="1234567890"
                                                        disabled />
                                                </div>
                                                <div class="mt-2">
                                                    <label class="form-label">Tempat, Tanggal Lahir</label>
                                                    <input type="text" class="form-control"
                                                        value="Bandar Lampung, 22 April 1996" disabled />
                                                </div>
                                                <div class="mt-2">
                                                    <label class="form-label">Agama</label>
                                                    <input type="text" class="form-control" value="Islam" disabled />
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="mt-2">
                                                    <label class="form-label">Alamat</label>
                                                    <textarea class="form-control" rows="4"
                                                        disabled>l. Radin Intan, Bandar Jaya Bar., Kec. Terbanggi Besar, Kabupaten Lampung Tengah, Lampung 34163</textarea>
                                                </div>
                                                <div class="mt-2">
                                                    <label class="form-label">Nomor Handphone</label>
                                                    <input type="text" class="form-control" value="1234567890"
                                                        disabled />
                                                </div>
                                                <div class="mt-2">
                                                    <label class="form-label">Email</label>
                                                    <input type="text" class="form-control"
                                                        value="rsiaputibungsu@gmail.com" disabled />
                                                </div>
                                                <div class="mt-2">
                                                    <label class="form-label">Golongan Darah</label>
                                                    <input type="text" class="form-control" value="0" disabled />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ about -->
                            </div>
                            <!--/ left profile info section -->
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection
