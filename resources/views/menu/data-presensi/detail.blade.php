@extends('layout.main')

@section('custom-css')
    <style></style>
@endsection

@section('custom-js')
    <script>
        $(function() {
            'use strict';
        })
    </script>
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-8 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">{{ $title }}</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Beranda</a>
                            </li>
                            <li class="breadcrumb-item active">Data Presensi
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header-right text-md-end col-md-4 col-12 d-md-block d-none">
            <button class="btn btn-outline-primary" type="button" onclick="history.back()">Kembali</button>
            <button class="btn btn-primary" type="button">
                <i data-feather="file" class="me-25"></i>
                <span>Eksport Data</span>
            </button>
        </div>
    </div>

    <div class="content-body">
        <div class="row" id="basic-table">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border">
                        <h4 class="title mb-0 text-uppercase">Filter</h4>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li>
                                    <a data-action="collapse" class=""><i data-feather="chevron-down"
                                            class="me-25"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse fade">
                        <div class="card-body">
                            <div class="row mt-2">
                                <div class="mb-1 col-md-4">
                                    <label class="form-label">Periode</label>
                                    <input type="date" class="form-control" />
                                </div>
                                <div class="mb-1 col-md-4">
                                    <label class="form-label">Sesi</label>
                                    <select class="select2 form-select w-100" id="select2-sesi" required>
                                        <option value="BP">Shift Pagi</option>
                                        <option value="IB">Shift Siang</option>
                                        <option value="SK">Shift Malam</option>
                                    </select>
                                </div>
                                <div class="mb-1 col-md-4">
                                    <label class="form-label">Status Lembur</label>
                                    <select class="select2 form-select w-100" id="select2-profesi" required>
                                        <option value="BP">Tidak</option>
                                        <option value="IB">Lembur</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <button type="button" class="btn btn-outline-primary me-1">
                                <span>Reset</span>
                            </button>
                            <button type="button" class="btn btn-primary">
                                <span>Cari</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="datatables-basic table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Sesi</th>
                                        <th>Waktu Sesi Masuk</th>
                                        <th>Waktu Sesi Pulang</th>
                                        <th>Waktu Masuk</th>
                                        <th>Waktu Pulang</th>
                                        <th>Selisih Waktu</th>
                                        <th>Keterangan</th>
                                        <th>Lembur</th>
                                        <th>Total Waktu Lembur</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Shift Pagi</td>
                                        <td>08:00:00</td>
                                        <td>14:00:00</td>
                                        <td>04-03-2022 07:35:43</td>
                                        <td>04-03-2022 14:05:12</td>
                                        <td>0 Menit</td>
                                        <td>-</td>
                                        <td><span class="badge bg-dark">Tidak</span></td>
                                        <td>0 Menit</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
