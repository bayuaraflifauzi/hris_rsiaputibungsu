@extends('layout.main')

@section('custom-css')
    <style></style>
@endsection

@section('custom-js')
    <script>
        $(function() {
            'use strict';
        })
    </script>
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">{{ $title }}</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Beranda</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Data Presensi</a>
                            </li>
                            <li class="breadcrumb-item active">{{ $title }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
            <div class="mb-1 breadcrumb-right">
                <button class="btn btn-primary" type="button">
                    <i data-feather="file" class="me-25"></i>
                    <span>Eksport Data</span>
                </button>
            </div>
        </div>
    </div>

    <div class="content-body">
        <div class="row" id="basic-table">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header border">
                        <h4 class="title mb-0 text-uppercase">Filter</h4>
                        <div class="heading-elements">
                            <ul class="list-inline mb-0">
                                <li>
                                    <a data-action="collapse" class=""><i data-feather="chevron-down"
                                            class="me-25"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="card-content collapse fade">
                        <div class="card-body">
                            <div class="row mt-2">
                                <div class="mb-1 col-md-6">
                                    <label class="form-label">Unit</label>
                                    <select class="select2 form-select w-100" id="select2-unit" required>
                                        <option value="BP">Unit 1</option>
                                        <option value="IB">Unit 2</option>
                                        <option value="SK">Unit 3</option>
                                    </select>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label">Jabatan</label>
                                    <select class="select2 form-select w-100" id="select2-jabatan" required>
                                        <option value="BP">Jabatan 1</option>
                                        <option value="IB">Jabatan 2</option>
                                        <option value="SK">Jabatan 3</option>
                                    </select>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label">Profesi</label>
                                    <select class="select2 form-select w-100" id="select2-profesi" required>
                                        <option value="BP">Profesi 1</option>
                                        <option value="IB">Profesi 2</option>
                                        <option value="SK">Profesi 3</option>
                                    </select>
                                </div>
                                <div class="mb-1 col-md-6">
                                    <label class="form-label">Nomor Induk Pegawai (NIP)</label>
                                    <input type="number" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="card-footer d-flex justify-content-end">
                            <button type="button" class="btn btn-outline-primary me-1">
                                <span>Reset</span>
                            </button>
                            <button type="button" class="btn btn-primary">
                                <span>Cari</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="datatables-basic table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NIP</th>
                                        <th>Nama Karyawan</th>
                                        <th>Unit</th>
                                        <th>Jabatan</th>
                                        <th>Profesi</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>1234</td>
                                        <td>Karyawan 1</td>
                                        <td>Unit 1</td>
                                        <td>Jabatan 1</td>
                                        <td>Profesi 1</td>
                                        <td>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-info"
                                                data-toggle="tooltip" data-placement="top" title="Detail"
                                                onclick="window.location = '{{ url('/data-presensi/detail') }}'">
                                                <i data-feather="eye"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>5678</td>
                                        <td>Karyawan 2</td>
                                        <td>Unit 2</td>
                                        <td>Jabatan 2</td>
                                        <td>Profesi 2</td>
                                        <td>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-info"
                                                data-toggle="tooltip" data-placement="top" title="Detail"
                                                onclick="window.location = '{{ url('/data-presensi/detail') }}'">
                                                <i data-feather="eye"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>7890</td>
                                        <td>Karyawan 3</td>
                                        <td>Unit 1</td>
                                        <td>Jabatan 1</td>
                                        <td>Profesi 3</td>
                                        <td>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-info"
                                                data-toggle="tooltip" data-placement="top" title="Detail"
                                                onclick="window.location = '{{ url('/data-presensi/detail') }}'">
                                                <i data-feather="eye"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
