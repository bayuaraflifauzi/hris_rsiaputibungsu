@extends('layout.main')

<link rel="stylesheet" type="text/css" href="app-assets/vendors/css/calendars/fullcalendar.min.css">
@section('custom-css')
    <style></style>
@endsection

@section('custom-js')
    <script></script>
@endsection


<link rel="stylesheet" type="text/css" href="app-assets/css/pages/app-calendar.css">
@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <!-- Dashboard Ecommerce Starts -->
        <section id="dashboard-ecommerce">
            <div class="row match-height">
                <!-- Medal Card -->
                {{-- <div class="col-xl-4 col-md-6 col-12">
                    <div class="card card-congratulation-medal">
                        <div class="card-body">
                            <h5>Congratulations 🎉 John!</h5>
                            <p class="card-text font-small-3">You have won gold medal</p>
                            <h3 class="mb-75 mt-2 pt-50">
                                <a href="#">$48.9k</a>
                            </h3>
                            <button type="button" class="btn btn-primary">View Sales</button>
                            <img src="{{ asset('app-assets/images/illustration/badge.svg') }}" class="congratulation-medal"
                                alt="Medal Pic" />
                        </div>
                    </div>
                </div> --}}
                <!--/ Medal Card -->

                <!-- Statistics Card -->
                <div class="col-xl-12 col-md-6 col-12">
                    <div class="card card-statistics">
                        <div class="card-header border">
                            <h4 class="card-title">Jumlah Pegawai</h4>
                            <div class="d-flex align-items-center">
                                {{-- <p class="card-text font-small-2 me-25 mb-0">Updated 1 month ago</p> --}}
                            </div>
                        </div>
                        <div class="card-body statistics-body">
                            <div class="row">
                                <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                    <div class="d-flex flex-row">
                                        <div class="avatar bg-light-primary me-2">
                                            <div class="avatar-content">
                                                <i data-feather="trending-up" class="avatar-icon"></i>
                                            </div>
                                        </div>
                                        <div class="my-auto">
                                            <h4 class="fw-bolder mb-0">230</h4>
                                            <p class="card-text font-small-3 mb-0">Unit 1</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-xl-0">
                                    <div class="d-flex flex-row">
                                        <div class="avatar bg-light-info me-2">
                                            <div class="avatar-content">
                                                <i data-feather="user" class="avatar-icon"></i>
                                            </div>
                                        </div>
                                        <div class="my-auto">
                                            <h4 class="fw-bolder mb-0">8.549</h4>
                                            <p class="card-text font-small-3 mb-0">Unit 2</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-sm-6 col-12 mb-2 mb-sm-0">
                                    <div class="d-flex flex-row">
                                        <div class="avatar bg-light-danger me-2">
                                            <div class="avatar-content">
                                                <i data-feather="box" class="avatar-icon"></i>
                                            </div>
                                        </div>
                                        <div class="my-auto">
                                            <h4 class="fw-bolder mb-0">1.423</h4>
                                            <p class="card-text font-small-3 mb-0">Unit 3</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-sm-6 col-12">
                                    <div class="d-flex flex-row">
                                        <div class="avatar bg-light-success me-2">
                                            <div class="avatar-content">
                                                <i data-feather="dollar-sign" class="avatar-icon"></i>
                                            </div>
                                        </div>
                                        <div class="my-auto">
                                            <h4 class="fw-bolder mb-0">9745</h4>
                                            <p class="card-text font-small-3 mb-0">Unit 4</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ Statistics Card -->
            </div>

            <div class="row match-height">
                <div class="col-lg-4 col-12">
                    <div class="row match-height">
                        <!-- Bar Chart - Orders -->
                        <div class="col-lg-6 col-md-3 col-6">
                            <div class="card">
                                <div class="card-body pb-50">
                                    <h6>Pegawai Aktif</h6>
                                    <h2 class="fw-bolder mb-1">276</h2>
                                    <div id="statistics-order-chart"></div>
                                </div>
                            </div>
                        </div>
                        <!--/ Bar Chart - Orders -->

                        <!-- Line Chart - Profit -->
                        <div class="col-lg-6 col-md-3 col-6">
                            <div class="card card-tiny-line-stats">
                                <div class="card-body pb-50">
                                    <h6>Pegawai Tidak Aktif</h6>
                                    <h2 class="fw-bolder mb-1">24</h2>
                                    <div id="statistics-profit-chart"></div>
                                </div>
                            </div>
                        </div>
                        <!--/ Line Chart - Profit -->

                        <!-- Earnings Card -->
                        <div class="col-lg-12 col-md-6 col-12">
                            <div class="card earnings-card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <h4 class="card-title mb-1">Pegawai Terverifikasi</h4>
                                            <p class="card-text text-muted font-small-2">
                                                <span class="fw-bolder">68.2%</span><span> Ijazah pendidikan pegawai
                                                    terverifikasi</span>
                                            </p>
                                        </div>
                                        <div class="col-6">
                                            <div id="earnings-chart"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Earnings Card -->
                    </div>
                </div>

                <!-- Revenue Report Card -->
                <div class="col-lg-8 col-12">
                    <div class="card card-revenue-budget">
                        <div class="row mx-0">
                            <div class="col-md-8 col-12 revenue-report-wrapper">
                                <div class="d-sm-flex justify-content-between align-items-center mb-3">
                                    <h4 class="card-title mb-50 mb-sm-0">Grafik Pertambahan Jumlah Karyawan</h4>
                                    <div class="d-flex align-items-center">
                                        <div class="d-flex align-items-center me-2">
                                            <span class="bullet bullet-primary font-small-3 me-50 cursor-pointer"></span>
                                            <span>Aktif</span>
                                        </div>
                                        <div class="d-flex align-items-center ms-75">
                                            <span class="bullet bullet-warning font-small-3 me-50 cursor-pointer"></span>
                                            <span>Tidak Aktif</span>
                                        </div>
                                    </div>
                                </div>
                                <div id="revenue-report-chart"></div>
                            </div>
                            <div class="col-md-4 col-12 budget-wrapper">
                                <div class="btn-group">
                                    <button type="button"
                                        class="btn btn-outline-primary btn-sm dropdown-toggle budget-dropdown"
                                        data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        2020
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#">2020</a>
                                        <a class="dropdown-item" href="#">2019</a>
                                        <a class="dropdown-item" href="#">2018</a>
                                    </div>
                                </div>
                                <div id="budget-chart"></div>
                                <button type="button" class="btn btn-primary">Increase Budget</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ Revenue Report Card -->
            </div>

            <div class="row match-height">
                <!-- Company Table Card -->
                <div class="col-lg-8 col-12">
                    <div class="card card-company-table">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Pengumuman</th>
                                            <th>Tanggal</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="fw-bolder">Pendaftaran Pelatihan di RSIA Puti
                                                            Bungsu</div>
                                                        <div class="font-small-2 text-muted">Pengumuman</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <span>4 Maret 2022</span>
                                                </div>
                                            </td>
                                            <td class="text-nowrap">
                                                <div class="d-flex flex-column">
                                                    <span class="fw-bolder mb-25">Aktif</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="avatar bg-light-primary me-1">
                                                    <div class="avatar-content">
                                                        <i data-feather="eye" class="font-medium-3"></i>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="fw-bolder">Pendaftaran Pelatihan di RSIA Puti
                                                            Bungsu</div>
                                                        <div class="font-small-2 text-muted">Pengumuman</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <span>4 Maret 2022</span>
                                                </div>
                                            </td>
                                            <td class="text-nowrap">
                                                <div class="d-flex flex-column">
                                                    <span class="fw-bolder mb-25">Aktif</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="avatar bg-light-primary me-1">
                                                    <div class="avatar-content">
                                                        <i data-feather="eye" class="font-medium-3"></i>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="fw-bolder">Pendaftaran Pelatihan di RSIA Puti
                                                            Bungsu</div>
                                                        <div class="font-small-2 text-muted">Pengumuman</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <span>4 Maret 2022</span>
                                                </div>
                                            </td>
                                            <td class="text-nowrap">
                                                <div class="d-flex flex-column">
                                                    <span class="fw-bolder mb-25">Aktif</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="avatar bg-light-primary me-1">
                                                    <div class="avatar-content">
                                                        <i data-feather="eye" class="font-medium-3"></i>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="fw-bolder">Pendaftaran Pelatihan di RSIA Puti
                                                            Bungsu</div>
                                                        <div class="font-small-2 text-muted">Pengumuman</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <span>4 Maret 2022</span>
                                                </div>
                                            </td>
                                            <td class="text-nowrap">
                                                <div class="d-flex flex-column">
                                                    <span class="fw-bolder mb-25">Aktif</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="avatar bg-light-primary me-1">
                                                    <div class="avatar-content">
                                                        <i data-feather="eye" class="font-medium-3"></i>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="fw-bolder">Pendaftaran Pelatihan di RSIA Puti
                                                            Bungsu</div>
                                                        <div class="font-small-2 text-muted">Pengumuman</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <span>4 Maret 2022</span>
                                                </div>
                                            </td>
                                            <td class="text-nowrap">
                                                <div class="d-flex flex-column">
                                                    <span class="fw-bolder mb-25">Aktif</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="avatar bg-light-primary me-1">
                                                    <div class="avatar-content">
                                                        <i data-feather="eye" class="font-medium-3"></i>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="fw-bolder">Pendaftaran Pelatihan di RSIA Puti
                                                            Bungsu</div>
                                                        <div class="font-small-2 text-muted">Pengumuman</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <span>4 Maret 2022</span>
                                                </div>
                                            </td>
                                            <td class="text-nowrap">
                                                <div class="d-flex flex-column">
                                                    <span class="fw-bolder mb-25">Aktif</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="avatar bg-light-primary me-1">
                                                    <div class="avatar-content">
                                                        <i data-feather="eye" class="font-medium-3"></i>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ Company Table Card -->

                <!-- Developer Meetup Card -->
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="card card-developer-meetup">
                        <div class="meetup-img-wrapper rounded-top text-center">
                            <img src="{{ asset('app-assets/images/illustration/email.svg') }}" alt="Meeting Pic"
                                height="170" />
                        </div>
                        <div class="card-body">
                            <div class="meetup-header d-flex align-items-center">
                                <div class="meetup-day">
                                    <h6 class="mb-0">THU</h6>
                                    <h3 class="mb-0">4</h3>
                                </div>
                                <div class="my-auto">
                                    <h4 class="card-title mb-25">Pendaftaran Pelatihan</h4>
                                    <p class="card-text mb-0">Pendaftaran Pelatihan di RSIA Puti Bungsu</p>
                                </div>
                            </div>
                            <div class="mt-0">
                                <div class="avatar float-start bg-light-primary rounded me-1">
                                    <div class="avatar-content">
                                        <i data-feather="calendar" class="avatar-icon font-medium-3"></i>
                                    </div>
                                </div>
                                <div class="more-info">
                                    <h6 class="mb-0">Jum'at 4 Maret 2022</h6>
                                    <small>10:AM to 6:PM</small>
                                </div>
                            </div>
                            <div class="mt-2">
                                <div class="avatar float-start bg-light-primary rounded me-1">
                                    <div class="avatar-content">
                                        <i data-feather="map-pin" class="avatar-icon font-medium-3"></i>
                                    </div>
                                </div>
                                <div class="more-info">
                                    <h6 class="mb-0">Aula RSIA Puti Bungsu</h6>
                                    <small>Lampung Tengah, Lampung</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-12">
                    <div class="card card-browser-states">
                        <div class="card-header">
                            <div>
                                <h4 class="card-title">Jadwal Pegawai</h4>
                                <p class="card-text font-small-2">Periode Maret 2022</p>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="app-calendar overflow-hidden border">
                                <div class="row g-0">
                                    <!-- Sidebar -->
                                    <div class="col app-calendar-sidebar flex-grow-0 overflow-hidden d-flex flex-column"
                                        id="app-calendar-sidebar">
                                        <div class="sidebar-wrapper">
                                            <div class="card-body d-flex justify-content-center">
                                                <button class="btn btn-primary btn-toggle-sidebar w-100"
                                                    data-bs-toggle="modal" data-bs-target="#add-new-sidebar">
                                                    <span class="align-middle">Lihat Jadwal Pegawai</span>
                                                </button>
                                            </div>
                                            <div class="card-body pb-0">
                                                <h5 class="section-label mb-1">
                                                    <span class="align-middle">Filter</span>
                                                </h5>
                                                <div class="form-check mb-1">
                                                    <input type="checkbox" class="form-check-input select-all"
                                                        id="select-all" checked />
                                                    <label class="form-check-label" for="select-all">Lihat Semua</label>
                                                </div>
                                                <div class="calendar-events-filter">
                                                    <div class="form-check form-check-danger mb-1">
                                                        <input type="checkbox" class="form-check-input input-filter"
                                                            id="personal" data-value="personal" checked />
                                                        <label class="form-check-label" for="personal">Unit 1</label>
                                                    </div>
                                                    <div class="form-check form-check-primary mb-1">
                                                        <input type="checkbox" class="form-check-input input-filter"
                                                            id="business" data-value="business" checked />
                                                        <label class="form-check-label" for="business">Unit 2</label>
                                                    </div>
                                                    <div class="form-check form-check-warning mb-1">
                                                        <input type="checkbox" class="form-check-input input-filter"
                                                            id="family" data-value="family" checked />
                                                        <label class="form-check-label" for="family">Unit 3</label>
                                                    </div>
                                                    <div class="form-check form-check-success mb-1">
                                                        <input type="checkbox" class="form-check-input input-filter"
                                                            id="holiday" data-value="holiday" checked />
                                                        <label class="form-check-label" for="holiday">Unit 4</label>
                                                    </div>
                                                    <div class="form-check form-check-info">
                                                        <input type="checkbox" class="form-check-input input-filter"
                                                            id="etc" data-value="etc" checked />
                                                        <label class="form-check-label" for="etc">Unit 5</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mt-auto">
                                            <img src="{{ asset('app-assets/images/illustration/email.svg') }}"
                                                alt="Calendar illustration" class="img-fluid" />
                                        </div>
                                    </div>
                                    <!-- /Sidebar -->

                                    <!-- Calendar -->
                                    <div class="col position-relative">
                                        <div class="card shadow-none border-0 mb-0 rounded-0">
                                            <div class="card-body pb-0">
                                                <div id="calendar"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Calendar -->
                                    <div class="body-content-overlay"></div>
                                </div>
                            </div>
                            <!-- Calendar Add/Update/Delete event modal-->
                            <div class="modal modal-slide-in event-sidebar fade" id="add-new-sidebar">
                                <div class="modal-dialog sidebar-lg">
                                    <div class="modal-content p-0">
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close">×</button>
                                        <div class="modal-header mb-1">
                                            <h5 class="modal-title">Add Event</h5>
                                        </div>
                                        <div class="modal-body flex-grow-1 pb-sm-0 pb-3">
                                            <form class="event-form needs-validation" data-ajax="false" novalidate>
                                                <div class="mb-1">
                                                    <label for="title" class="form-label">Title</label>
                                                    <input type="text" class="form-control" id="title" name="title"
                                                        placeholder="Event Title" required />
                                                </div>
                                                <div class="mb-1">
                                                    <label for="select-label" class="form-label">Label</label>
                                                    <select class="select2 select-label form-select w-100"
                                                        id="select-label" name="select-label">
                                                        <option data-label="primary" value="Business" selected>Business
                                                        </option>
                                                        <option data-label="danger" value="Personal">Personal</option>
                                                        <option data-label="warning" value="Family">Family</option>
                                                        <option data-label="success" value="Holiday">Holiday</option>
                                                        <option data-label="info" value="ETC">ETC</option>
                                                    </select>
                                                </div>
                                                <div class="mb-1 position-relative">
                                                    <label for="start-date" class="form-label">Start Date</label>
                                                    <input type="text" class="form-control" id="start-date"
                                                        name="start-date" placeholder="Start Date" />
                                                </div>
                                                <div class="mb-1 position-relative">
                                                    <label for="end-date" class="form-label">End Date</label>
                                                    <input type="text" class="form-control" id="end-date" name="end-date"
                                                        placeholder="End Date" />
                                                </div>
                                                <div class="mb-1">
                                                    <div class="form-check form-switch">
                                                        <input type="checkbox" class="form-check-input allDay-switch"
                                                            id="customSwitch3" />
                                                        <label class="form-check-label" for="customSwitch3">All Day</label>
                                                    </div>
                                                </div>
                                                <div class="mb-1">
                                                    <label for="event-url" class="form-label">Event URL</label>
                                                    <input type="url" class="form-control" id="event-url"
                                                        placeholder="https://www.google.com" />
                                                </div>
                                                <div class="mb-1 select2-primary">
                                                    <label for="event-guests" class="form-label">Add Guests</label>
                                                    <select class="select2 select-add-guests form-select w-100"
                                                        id="event-guests" multiple>
                                                        <option data-avatar="1-small.png" value="Jane Foster">Jane Foster
                                                        </option>
                                                        <option data-avatar="3-small.png" value="Donna Frank">Donna Frank
                                                        </option>
                                                        <option data-avatar="5-small.png" value="Gabrielle Robertson">
                                                            Gabrielle Robertson</option>
                                                        <option data-avatar="7-small.png" value="Lori Spears">Lori Spears
                                                        </option>
                                                        <option data-avatar="9-small.png" value="Sandy Vega">Sandy Vega
                                                        </option>
                                                        <option data-avatar="11-small.png" value="Cheryl May">Cheryl May
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="mb-1">
                                                    <label for="event-location" class="form-label">Location</label>
                                                    <input type="text" class="form-control" id="event-location"
                                                        placeholder="Enter Location" />
                                                </div>
                                                <div class="mb-1">
                                                    <label class="form-label">Description</label>
                                                    <textarea name="event-description-editor" id="event-description-editor"
                                                        class="form-control"></textarea>
                                                </div>
                                                <div class="mb-1 d-flex">
                                                    <button type="submit"
                                                        class="btn btn-primary add-event-btn me-1">Add</button>
                                                    <button type="button" class="btn btn-outline-secondary btn-cancel"
                                                        data-bs-dismiss="modal">Cancel</button>
                                                    <button type="submit"
                                                        class="btn btn-primary update-event-btn d-none me-1">Update</button>
                                                    <button
                                                        class="btn btn-outline-danger btn-delete-event d-none">Delete</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--/ Developer Meetup Card -->

                <!-- Browser States Card -->
                {{-- <div class="col-lg-4 col-md-6 col-12">
                    <div class="card card-browser-states">
                        <div class="card-header">
                            <div>
                                <h4 class="card-title">Browser States</h4>
                                <p class="card-text font-small-2">Counter August 2020</p>
                            </div>
                            <div class="dropdown chart-dropdown">
                                <i data-feather="more-vertical" class="font-medium-3 cursor-pointer"
                                    data-bs-toggle="dropdown"></i>
                                <div class="dropdown-menu dropdown-menu-end">
                                    <a class="dropdown-item" href="#">Last 28 Days</a>
                                    <a class="dropdown-item" href="#">Last Month</a>
                                    <a class="dropdown-item" href="#">Last Year</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="browser-states">
                                <div class="d-flex">
                                    <img src="{{ asset('app-assets/images/icons/google-chrome.png') }}"
                                        class="rounded me-1" height="30" alt="Google Chrome" />
                                    <h6 class="align-self-center mb-0">Google Chrome</h6>
                                </div>
                                <div class="d-flex align-items-center">
                                    <div class="fw-bold text-body-heading me-1">54.4%</div>
                                    <div id="browser-state-chart-primary"></div>
                                </div>
                            </div>
                            <div class="browser-states">
                                <div class="d-flex">
                                    <img src="{{ asset('app-assets/images/icons/mozila-firefox.png') }}"
                                        class="rounded me-1" height="30" alt="Mozila Firefox" />
                                    <h6 class="align-self-center mb-0">Mozila Firefox</h6>
                                </div>
                                <div class="d-flex align-items-center">
                                    <div class="fw-bold text-body-heading me-1">6.1%</div>
                                    <div id="browser-state-chart-warning"></div>
                                </div>
                            </div>
                            <div class="browser-states">
                                <div class="d-flex">
                                    <img src="{{ asset('app-assets/images/icons/apple-safari.png') }}"
                                        class="rounded me-1" height="30" alt="Apple Safari" />
                                    <h6 class="align-self-center mb-0">Apple Safari</h6>
                                </div>
                                <div class="d-flex align-items-center">
                                    <div class="fw-bold text-body-heading me-1">14.6%</div>
                                    <div id="browser-state-chart-secondary"></div>
                                </div>
                            </div>
                            <div class="browser-states">
                                <div class="d-flex">
                                    <img src="{{ asset('app-assets/images/icons/internet-explorer.png') }}"
                                        class="rounded me-1" height="30" alt="Internet Explorer" />
                                    <h6 class="align-self-center mb-0">Internet Explorer</h6>
                                </div>
                                <div class="d-flex align-items-center">
                                    <div class="fw-bold text-body-heading me-1">4.2%</div>
                                    <div id="browser-state-chart-info"></div>
                                </div>
                            </div>
                            <div class="browser-states">
                                <div class="d-flex">
                                    <img src="{{ asset('app-assets/images/icons/opera.png') }}" class="rounded me-1"
                                        height="30" alt="Opera Mini" />
                                    <h6 class="align-self-center mb-0">Opera Mini</h6>
                                </div>
                                <div class="d-flex align-items-center">
                                    <div class="fw-bold text-body-heading me-1">8.4%</div>
                                    <div id="browser-state-chart-danger"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <!--/ Browser States Card -->

                <!-- Goal Overview Card -->
                {{-- <div class="col-lg-4 col-md-6 col-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="card-title">Goal Overview</h4>
                            <i data-feather="help-circle" class="font-medium-3 text-muted cursor-pointer"></i>
                        </div>
                        <div class="card-body p-0">
                            <div id="goal-overview-radial-bar-chart" class="my-2"></div>
                            <div class="row border-top text-center mx-0">
                                <div class="col-6 border-end py-1">
                                    <p class="card-text text-muted mb-0">Completed</p>
                                    <h3 class="fw-bolder mb-0">786,617</h3>
                                </div>
                                <div class="col-6 py-1">
                                    <p class="card-text text-muted mb-0">In Progress</p>
                                    <h3 class="fw-bolder mb-0">13,561</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <!--/ Goal Overview Card -->

                <!-- Transaction Card -->
                {{-- <div class="col-lg-4 col-md-6 col-12">
                    <div class="card card-transaction">
                        <div class="card-header">
                            <h4 class="card-title">Transactions</h4>
                            <div class="dropdown chart-dropdown">
                                <i data-feather="more-vertical" class="font-medium-3 cursor-pointer"
                                    data-bs-toggle="dropdown"></i>
                                <div class="dropdown-menu dropdown-menu-end">
                                    <a class="dropdown-item" href="#">Last 28 Days</a>
                                    <a class="dropdown-item" href="#">Last Month</a>
                                    <a class="dropdown-item" href="#">Last Year</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="transaction-item">
                                <div class="d-flex">
                                    <div class="avatar bg-light-primary rounded float-start">
                                        <div class="avatar-content">
                                            <i data-feather="pocket" class="avatar-icon font-medium-3"></i>
                                        </div>
                                    </div>
                                    <div class="transaction-percentage">
                                        <h6 class="transaction-title">Wallet</h6>
                                        <small>Starbucks</small>
                                    </div>
                                </div>
                                <div class="fw-bolder text-danger">- $74</div>
                            </div>
                            <div class="transaction-item">
                                <div class="d-flex">
                                    <div class="avatar bg-light-success rounded float-start">
                                        <div class="avatar-content">
                                            <i data-feather="check" class="avatar-icon font-medium-3"></i>
                                        </div>
                                    </div>
                                    <div class="transaction-percentage">
                                        <h6 class="transaction-title">Bank Transfer</h6>
                                        <small>Add Money</small>
                                    </div>
                                </div>
                                <div class="fw-bolder text-success">+ $480</div>
                            </div>
                            <div class="transaction-item">
                                <div class="d-flex">
                                    <div class="avatar bg-light-danger rounded float-start">
                                        <div class="avatar-content">
                                            <i data-feather="dollar-sign" class="avatar-icon font-medium-3"></i>
                                        </div>
                                    </div>
                                    <div class="transaction-percentage">
                                        <h6 class="transaction-title">Paypal</h6>
                                        <small>Add Money</small>
                                    </div>
                                </div>
                                <div class="fw-bolder text-success">+ $590</div>
                            </div>
                            <div class="transaction-item">
                                <div class="d-flex">
                                    <div class="avatar bg-light-warning rounded float-start">
                                        <div class="avatar-content">
                                            <i data-feather="credit-card" class="avatar-icon font-medium-3"></i>
                                        </div>
                                    </div>
                                    <div class="transaction-percentage">
                                        <h6 class="transaction-title">Mastercard</h6>
                                        <small>Ordered Food</small>
                                    </div>
                                </div>
                                <div class="fw-bolder text-danger">- $23</div>
                            </div>
                            <div class="transaction-item">
                                <div class="d-flex">
                                    <div class="avatar bg-light-info rounded float-start">
                                        <div class="avatar-content">
                                            <i data-feather="trending-up" class="avatar-icon font-medium-3"></i>
                                        </div>
                                    </div>
                                    <div class="transaction-percentage">
                                        <h6 class="transaction-title">Transfer</h6>
                                        <small>Refund</small>
                                    </div>
                                </div>
                                <div class="fw-bolder text-success">+ $98</div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <!--/ Transaction Card -->
            </div>
        </section>
        <!-- Dashboard Ecommerce ends -->

    </div>

    <script src="app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="app-assets/vendors/js/calendar/fullcalendar.min.js"></script>
    <script src="app-assets/vendors/js/extensions/moment.min.js"></script>
    <script src="app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script src="app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
    <script src="app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="app-assets/js/scripts/pages/app-calendar-events.js"></script>
    <script src="app-assets/js/scripts/pages/app-calendar.js"></script>
@endsection
