@extends('layout.main')

@section('custom-css')
    <style>
        .card-statistics .statistics-body {
            padding: 2rem 2.4rem !important;
        }

        @media (max-width: 991.98px) {

            .card-statistics .card-header,
            .card-statistics .statistics-body {
                padding: 1.5rem !important;
            }
        }

        .card-company-table thead th {
            border: 0;
        }

        .card-company-table td {
            padding-top: 0.75rem;
            padding-bottom: 0.75rem;
        }

        .card-company-table td .avatar {
            background-color: #f8f8f8;
            margin-right: 2rem;
        }

        .card-company-table td .avatar img {
            border-radius: 0;
        }

        .card-browser-states .browser-states:first-child {
            margin-top: 0;
        }

        .card-browser-states .browser-states:not(:first-child) {
            margin-top: 1.7rem;
        }

        .card-transaction .transaction-item:not(:last-child) {
            margin-bottom: 1.5rem;
        }

    </style>
@endsection

@section('custom-js')
    <script></script>
@endsection

@section('content')
    <div class="content-header row">
    </div>
    <div class="content-body">
        <!-- Dashboard Ecommerce Starts -->
        <section class="app-user-view">
            <!-- User Card & Plan Starts -->
            <div class="row">
                <div class="col-xl-12">
                    <div class="text-center">
                        <h1 class="title mb-1">Halo! Desi Oktarika</h1>
                    </div>
                </div>

                <!-- Plan Card starts-->
                <div class="col-xl-4 col-md-6 col-12">
                    <div class="card plan-card border-primary">
                        <div class="card-header justify-content-center">
                            <h4 class="card-title text-center">Kamis, 24 Februari 2022 <div style="font-size: 32px">09:45:23
                                </div>
                            </h4>
                        </div>
                        <div class="card-body p-0" style="position: relative;">
                            <div class="row border-top text-center mx-0">
                                <div class="col-6 border-end py-1">
                                    <p class="card-text text-muted mb-0">Waktu Kehadiran</p>
                                    <h3 class="fw-bolder mb-0">07:45:23</h3>
                                </div>
                                <div class="col-6 py-1">
                                    <p class="card-text text-muted mb-0">Menit Kerja</p>
                                    <h3 class="fw-bolder mb-0">120 Menit</h3>
                                </div>
                                <div class="col-12 border-top py-1">
                                    <p class="card-text text-muted mb-0">Lokasi Masuk</p>
                                    <h3 class="fw-bolder mb-0">Jalan Suka Rasa No. 108</h3>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="button" class="btn btn-dark w-100" data-bs-toggle="modal" data-bs-target="#absen-pulang">
                                    <span>ABSEN PULANG</span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade text-start" id="absen-pulang" tabindex="-1" aria-labelledby="myModalLabel1"
                        aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel33">Konfirmasi Pulang</h4>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                                </div>
                                <form action="#">
                                    <div class="modal-body">
                                        <div class="row justify-content-center">
                                            <div class="mb-1 col-md-8">
                                                <label class="form-label">Perihal Absen Pulang</label>
                                                <div class="item-align-start">
                                                    <div class="form-check form-check-inline p-0">
                                                        <input class="form-check-input1" type="radio" name="inlineRadioOptions"
                                                            id="inlineRadio4" value="option4" checked="">
                                                        <label class="form-check-label" for="inlineRadio4">Izin Pulang Cepat</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input1" type="radio" name="inlineRadioOptions"
                                                            id="inlineRadio5" value="option5">
                                                        <label class="form-check-label" for="inlineRadio5">Lembur</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input1" type="radio" name="inlineRadioOptions"
                                                            id="inlineRadio6" value="option6">
                                                        <label class="form-check-label" for="inlineRadio6">Perjalanan Dinas</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <section id="izin-pulang-cepat">

                                            </section>

                                            <section id="lembur">

                                            </section>

                                            <section id="perjalanan-dinas">
                                                
                                            </section>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-secondary"
                                            data-bs-dismiss="modal">Batal</button>
                                        <button type="button" class="btn btn-primary"
                                            data-bs-dismiss="modal">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Plan CardEnds -->

                <!-- User Card starts-->
                <div class="col-xl-8 col-md-6 col-12">
                    <div class="card card-developer-meetup card-statistics">
                        <div class="meetup-img-wrapper rounded-top text-end">
                            <img src="{{ asset('app-assets/images/illustration/demand.svg') }}" alt="Meeting Pic"
                                height="170" />
                        </div>
                        <div class="card-body statistics-body">
                            <div class="row">
                                <div class="col-md-4 mb-1">
                                    <div class="d-flex flex-row">
                                        <div class="avatar bg-light-primary me-2">
                                            <div class="avatar-content">
                                                <i data-feather="trending-up" class="avatar-icon"></i>
                                            </div>
                                        </div>
                                        <div class="my-auto">
                                            <h5 class="fw-bolder mb-0">Perawat</h5>
                                            <p class="card-text font-small-3 mb-0">Profesi</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-1">
                                    <div class="d-flex flex-row">
                                        <div class="avatar bg-light-info me-2">
                                            <div class="avatar-content">
                                                <i data-feather="user" class="avatar-icon"></i>
                                            </div>
                                        </div>
                                        <div class="my-auto">
                                            <h5 class="fw-bolder mb-0">Pelaksana</h5>
                                            <p class="card-text font-small-3 mb-0">Jabatan</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="d-flex flex-row">
                                        <div class="avatar bg-light-danger me-2">
                                            <div class="avatar-content">
                                                <i data-feather="box" class="avatar-icon"></i>
                                            </div>
                                        </div>
                                        <div class="my-auto">
                                            <h5 class="fw-bolder mb-0">Unit Rawat Jalan</h5>
                                            <p class="card-text font-small-3 mb-0">Unit</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /User Card Ends-->
            </div>

            <div class="row">
                <!-- Company Table Card -->
                <div class="col-lg-8 col-12">
                    <div class="card card-company-table">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Pengumuman</th>
                                            <th>Tanggal Terbit</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="fw-bolder">Pendaftaran Pelatihan di RSIA Puti
                                                            Bungsu</div>
                                                        <div class="font-small-2 text-muted">Pengumuman</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <span>4 Maret 2022</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="avatar bg-light-primary me-1">
                                                    <div class="avatar-content">
                                                        <i data-feather="eye" class="font-medium-3"></i>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="fw-bolder">Pendaftaran Pelatihan di RSIA Puti
                                                            Bungsu</div>
                                                        <div class="font-small-2 text-muted">Pengumuman</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <span>4 Maret 2022</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="avatar bg-light-primary me-1">
                                                    <div class="avatar-content">
                                                        <i data-feather="eye" class="font-medium-3"></i>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="fw-bolder">Pendaftaran Pelatihan di RSIA Puti
                                                            Bungsu</div>
                                                        <div class="font-small-2 text-muted">Pengumuman</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <span>4 Maret 2022</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="avatar bg-light-primary me-1">
                                                    <div class="avatar-content">
                                                        <i data-feather="eye" class="font-medium-3"></i>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="fw-bolder">Pendaftaran Pelatihan di RSIA Puti
                                                            Bungsu</div>
                                                        <div class="font-small-2 text-muted">Pengumuman</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <span>4 Maret 2022</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="avatar bg-light-primary me-1">
                                                    <div class="avatar-content">
                                                        <i data-feather="eye" class="font-medium-3"></i>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="fw-bolder">Pendaftaran Pelatihan di RSIA Puti
                                                            Bungsu</div>
                                                        <div class="font-small-2 text-muted">Pengumuman</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <span>4 Maret 2022</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="avatar bg-light-primary me-1">
                                                    <div class="avatar-content">
                                                        <i data-feather="eye" class="font-medium-3"></i>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <div>
                                                        <div class="fw-bolder">Pendaftaran Pelatihan di RSIA Puti
                                                            Bungsu</div>
                                                        <div class="font-small-2 text-muted">Pengumuman</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex align-items-center">
                                                    <span>4 Maret 2022</span>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="avatar bg-light-primary me-1">
                                                    <div class="avatar-content">
                                                        <i data-feather="eye" class="font-medium-3"></i>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ Company Table Card -->

                <!-- Developer Meetup Card -->
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="card card-developer-meetup">
                        <div class="meetup-img-wrapper rounded-top text-center">
                            <img src="{{ asset('app-assets/images/illustration/email.svg') }}" alt="Meeting Pic"
                                height="170" />
                        </div>
                        <div class="card-body">
                            <div class="meetup-header d-flex align-items-center">
                                <div class="meetup-day">
                                    <h6 class="mb-0">THU</h6>
                                    <h3 class="mb-0">4</h3>
                                </div>
                                <div class="my-auto">
                                    <h4 class="card-title mb-25">Pendaftaran Pelatihan</h4>
                                    <p class="card-text mb-0">Pendaftaran Pelatihan di RSIA Puti Bungsu</p>
                                </div>
                            </div>
                            <div class="mt-0">
                                <div class="avatar float-start bg-light-primary rounded me-1">
                                    <div class="avatar-content">
                                        <i data-feather="calendar" class="avatar-icon font-medium-3"></i>
                                    </div>
                                </div>
                                <div class="more-info">
                                    <h6 class="mb-0">Jum'at 4 Maret 2022</h6>
                                    <small>10:AM to 6:PM</small>
                                </div>
                            </div>
                            <div class="mt-2">
                                <div class="avatar float-start bg-light-primary rounded me-1">
                                    <div class="avatar-content">
                                        <i data-feather="map-pin" class="avatar-icon font-medium-3"></i>
                                    </div>
                                </div>
                                <div class="more-info">
                                    <h6 class="mb-0">Aula RSIA Puti Bungsu</h6>
                                    <small>Lampung Tengah, Lampung</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header border">
                            <h2 class="title mb-0 text-uppercase">Data Presensi</h2>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="datatables-basic table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Sesi</th>
                                            <th>Waktu Sesi Masuk</th>
                                            <th>Waktu Sesi Pulang</th>
                                            <th>Waktu Masuk</th>
                                            <th>Waktu Pulang</th>
                                            <th>Selisih Waktu</th>
                                            <th>Keterangan</th>
                                            <th>Lembur</th>
                                            <th>Total Waktu Lembur</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Shift Pagi</td>
                                            <td>08:00:00</td>
                                            <td>14:00:00</td>
                                            <td>04-03-2022 07:35:43</td>
                                            <td>04-03-2022 14:05:12</td>
                                            <td>0 Menit</td>
                                            <td>-</td>
                                            <td><span class="badge bg-dark">Tidak</span></td>
                                            <td>0 Menit</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- User Card & Plan Ends -->
        </section>
        <!-- Dashboard Ecommerce ends -->

    </div>
@endsection
