@extends('layout.main')

@section('custom-css')
    <style></style>
@endsection

@section('custom-js')
    <script>
        $(function() {
            'use strict';
        })
    </script>
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-8 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">{{ $title }}</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Beranda</a>
                            </li>
                            <li class="breadcrumb-item acvite">Data Pengelolaan
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header-right text-md-end col-md-4 col-12 d-md-block d-none">
            <div class="mb-1 breadcrumb-right">
                <button class="btn btn-outline-primary" type="button">
                    <i data-feather="upload" class="me-25"></i>
                    <span>Import Data Presensi Karyawan</span>
                </button>
            </div>
        </div>
    </div>

    <div class="content-body">
        <div class="row" id="basic-table">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header row">
                        <div class="col-md-6">
                            <input type="month" class="form-control"/>
                        </div>
                        <div class="col-md-6 d-flex justify-content-end">
                            <button class="btn btn-primary" type="button">
                                <i data-feather="edit" class="me-25"></i>
                                <span>Kelola Data Presensi</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered mb-1">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-nowrap align-middle text-center" rowspan="3">NO</th>
                                        <th scope="col" class="text-nowrap align-middle text-center" rowspan="3">NAMA</th>
                                        <th scope="col" class="text-nowrap align-middle text-center" rowspan="3">NIP</th>
                                        <th scope="col" class="text-nowrap align-middle text-center" rowspan="3">UNIT</th>
                                        <th scope="col" class="text-nowrap align-middle text-center" rowspan="3">JABATAN</th>
                                        <th scope="col" class="text-nowrap align-middle text-center" rowspan="3">PROFESI</th>
                                        <th scope="col" class="text-nowrap align-middle text-center" colspan="8">FEBRUARI 2022</th>
                                    </tr>
                                    <tr>
                                        
                                        <th scope="col" class="text-nowrap align-middle text-center">SL</th>
                                        <th scope="col" class="text-nowrap align-middle text-center">RB</th>
                                        <th scope="col" class="text-nowrap align-middle text-center">KM</th>
                                        <th scope="col" class="text-nowrap align-middle text-center">JM</th>
                                        <th scope="col" class="text-nowrap align-middle text-center" style="color: white; background-color: darkmagenta">SB</th>
                                        <th scope="col" class="text-nowrap align-middle text-center">MG</th>
                                        <th scope="col" class="text-nowrap align-middle text-center">SN</th>
                                        <th scope="col" class="text-nowrap align-middle text-center">SL</th>
                                    </tr>
                                    <tr>
                                        
                                        <th scope="col" class="text-nowrap align-middle text-center">1</th>
                                        <th scope="col" class="text-nowrap align-middle text-center">2</th>
                                        <th scope="col" class="text-nowrap align-middle text-center">3</th>
                                        <th scope="col" class="text-nowrap align-middle text-center">4</th>
                                        <th scope="col" class="text-nowrap align-middle text-center" style="color: white; background-color: darkmagenta">5</th>
                                        <th scope="col" class="text-nowrap align-middle text-center">6</th>
                                        <th scope="col" class="text-nowrap align-middle text-center">7</th>
                                        <th scope="col" class="text-nowrap align-middle text-center">8</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-nowrap align-middle text-center">1</td>
                                        <td class="text-nowrap align-middle">
                                            <button type="button" class="btn btn-flat-primary waves-effect" data-bs-toggle="modal" data-bs-target="#edit">Desi Oktarika</button>
                                        </td>
                                        <td class="text-nowrap align-middle">1234567890</td>
                                        <td class="text-nowrap align-middle">Unit Rawai Inap</td>
                                        <td class="text-nowrap align-middle">Pelaksana</td>
                                        <td class="text-nowrap align-middle">Perawat</td>
                                        <td class="text-nowrap align-middle text-center bg-info">P</td>
                                        <td class="text-nowrap align-middle text-center bg-info">P</td>
                                        <td class="text-nowrap align-middle text-center bg-warning">S</td>
                                        <td class="text-nowrap align-middle text-center bg-warning">S</td>
                                        <td class="text-nowrap align-middle text-center bg-success">M</td>
                                        <td class="text-nowrap align-middle text-center bg-success">M</td>
                                        <td class="text-nowrap align-middle text-center bg-danger">L</td>
                                        <td class="text-nowrap align-middle text-center bg-danger">L</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade text-start edit" id="edit" tabindex="-1" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel33">Kelola Data Presensi Desi Oktarika</h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="#">
                    <div class="modal-body">
                        <div class="row">
                            <div class="mb-1 col-md-12">
                                <label class="form-label">Kebutuhan Kelola</label>
                                <select class="select2 form-select w-100" id="select2-kelola" required>
                                    <option value="BP">Ubah Jadwal</option>
                                    <option value="IB">Tukar Jadwal</option>
                                    <option value="SK">Izin</option>
                                </select>
                            </div>
                            <div class="mb-1 col-md-12">
                                <label class="form-label">Tanggal yang dikelola</label>
                                <input type="date" class="form-control" />
                            </div>
                            <div class="mb-1 col-md-12">
                                <label class="form-label">Karyawan Pengganti</label>
                                <select class="select2 form-select w-100" id="select2-karyawan" required>
                                    <option value="BP">Karyawan 1</option>
                                    <option value="IB">Karyawan 2</option>
                                    <option value="SK">Karyawan 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Batal</button>
                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
