@extends('layout.main')

@section('custom-css')
    <style></style>
@endsection

@section('custom-js')
    <script>
        $(function() {
            'use strict';
        })
    </script>
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-md-12">
                    <h2 class="content-header-title float-start mb-0">{{ $title }}</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Beranda</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Data Pengelolaan</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Dokumen Legalitas</a>
                            </li>
                            <li class="breadcrumb-item active">{{ $title }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
            <div class="mb-1 breadcrumb-right">
                <button class="btn btn-outline-primary" type="button" onclick="history.back()">Kembali</button>
            </div>
        </div>
    </div>

    <div class="content-body">
        <div class="row" id="basic-table">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="datatables-basic table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Karyawan</th>
                                        <th>Unit</th>
                                        <th>Jabatan</th>
                                        <th>No. Dokumen</th>
                                        <th>Masa Berlaku</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Karyawan 1</td>
                                        <td>Unit 1</td>
                                        <td>Jabatan 1</td>
                                        <td>1234567890</td>
                                        <td>27 Juli 2022</td>
                                        <td>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-primary"
                                                data-toggle="tooltip" data-placement="top" title="Download">
                                                <i data-feather="download"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Karyawan 2</td>
                                        <td>Unit 2</td>
                                        <td>Jabatan 2</td>
                                        <td>1234567890</td>
                                        <td>27 Februari 2022 <i data-feather='clock' class="text-danger"></i></td>
                                        <td>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-primary"
                                                data-toggle="tooltip" data-placement="top" title="Download">
                                                <i data-feather="download"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Karyawan 3</td>
                                        <td>Unit 1</td>
                                        <td>Jabatan 1</td>
                                        <td>1234567890</td>
                                        <td>28 Februari 2022 <i data-feather='clock' class="text-danger"></i></td>
                                        <td>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-primary"
                                                data-toggle="tooltip" data-placement="top" title="Download">
                                                <i data-feather="download"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
