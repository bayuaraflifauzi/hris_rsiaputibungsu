@extends('layout.main')

@section('custom-css')
    <style></style>
@endsection

@section('custom-js')
    <script>
        $(function() {
            'use strict';
        })
    </script>
@endsection

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">{{ $title }}</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Beranda</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#">Data Pengelolaan</a>
                            </li>
                            <li class="breadcrumb-item active">{{ $title }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-header-right text-md-end col-md-3 col-12 d-md-block d-none">
            <div class="mb-1 breadcrumb-right">
                <div class="dropdown">
                    <button class="btn-icon btn btn-primary btn-round btn-sm dropdown-toggle" type="button"
                        data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i
                            data-feather="grid"></i></button>
                    <div class="dropdown-menu dropdown-menu-end">
                        <a class="dropdown-item" href="#"><i class="me-1" data-feather="filter"></i><span
                                class="align-middle">Filter</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content-body">
        <div class="row" id="basic-table">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="datatables-basic table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Dokumen Legalitas</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Dokumen 1</td>
                                        <td>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-info"
                                                data-toggle="tooltip" data-placement="top" title="Detail" onclick="window.location = '{{ url('/dokumen-legalitas/detail') }}'">
                                                <i data-feather="eye"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Dokumen 2</td>
                                        <td>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-info"
                                                data-toggle="tooltip" data-placement="top" title="Detail" onclick="window.location = '{{ url('/dokumen-legalitas/detail') }}'">
                                                <i data-feather="eye"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Dokumen 3</td>
                                        <td>
                                            <button type="button" class="btn btn-icon rounded-circle btn-outline-info"
                                                data-toggle="tooltip" data-placement="top" title="Detail" onclick="window.location = '{{ url('/dokumen-legalitas/detail') }}'">
                                                <i data-feather="eye"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
