<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/dashboard', function () {
    $data = array(
        'title' => 'Dashboard',
        'menu' => 'dashboard',
        'submenu' => ''
    );
    return view('menu.dashboard.dashboard', $data);
});

Route::get('/dashboard-direksi', function () {
    $data = array(
        'title' => 'Dashboard Direksi',
        'menu' => 'dashboard-direksi',
        'submenu' => ''
    );
    return view('menu.dashboard.dashboard-direksi', $data);
});

Route::get('/personal-data', function () {
    $data = array(
        'title' => 'Personal Data',
        'menu' => 'personal-data',
        'submenu' => ''
    );
    return view('menu.personal-data.index', $data);
});

Route::get('/data-unit', function () {
    $data = array(
        'title' => 'Data Unit',
        'menu' => 'data-unit',
        'submenu' => ''
    );
    return view('menu.data-unit.index', $data);
});

Route::get('/data-unit/settings', function () {
    $data = array(
        'title' => 'Pengaturan Unit 1',
        'menu' => 'data-unit',
        'submenu' => ''
    );
    return view('menu.data-unit.settings', $data);
});

Route::get('/data-karyawan', function () {
    $data = array(
        'title' => 'Data Karyawan',
        'menu' => 'data-karyawan',
        'submenu' => ''
    );
    return view('menu.data-karyawan.index', $data);
});

Route::get('/data-karyawan/detail', function () {
    $data = array(
        'title' => 'Detail Data Karyawan',
        'menu' => 'data-karyawan',
        'submenu' => ''
    );
    return view('menu.data-karyawan.detail', $data);
});

Route::get('/data-karyawan/tambah-data', function () {
    $data = array(
        'title' => 'Tambah Data Karyawan',
        'menu' => 'data-karyawan',
        'submenu' => ''
    );
    return view('menu.data-karyawan.add-karyawan', $data);
});

Route::get('/data-dokumen-legalitas', function () {
    $data = array(
        'title' => 'Data Dokumen Legalitas',
        'menu' => 'data-dokumen-legalitas',
        'submenu' => ''
    );
    return view('menu.data-dokumen-legalitas.index', $data);
});

Route::get('/dokumen-legalitas', function () {
    $data = array(
        'title' => 'Dokumen Legalitas',
        'menu' => 'dokumen-legalitas',
        'submenu' => ''
    );
    return view('menu.dokumen-legalitas.index', $data);
});

Route::get('/dokumen-legalitas/detail', function () {
    $data = array(
        'title' => 'Dokumen Legalitas A',
        'menu' => 'dokumen-legalitas',
        'submenu' => ''
    );
    return view('menu.dokumen-legalitas.detail', $data);
});

Route::get('/data-pelatihan-diklat', function () {
    $data = array(
        'title' => 'Data Kategori Pelatihan & Diklat',
        'menu' => 'data-kategori',
        'submenu' => 'data-diklat'
    );
    return view('menu.data-diklat.index', $data);
});

Route::get('/data-profesi', function () {
    $data = array(
        'title' => 'Data Kategori Profesi',
        'menu' => 'data-kategori',
        'submenu' => 'data-profesi'
    );
    return view('menu.data-profesi.index', $data);
});

Route::get('/data-tunjangan', function () {
    $data = array(
        'title' => 'Data Kategori Tunjangan',
        'menu' => 'data-kategori',
        'submenu' => 'data-tunjangan'
    );
    return view('menu.data-tunjangan.index', $data);
});

Route::get('/data-jabatan', function () {
    $data = array(
        'title' => 'Data Jabatan',
        'menu' => 'data-kategori',
        'submenu' => 'data-jabatan'
    );
    return view('menu.data-jabatan.index', $data);
});

Route::get('/data-presensi', function () {
    $data = array(
        'title' => 'Data Presensi Karyawan',
        'menu' => 'data-presensi',
        'submenu' => ''
    );
    return view('menu.data-presensi.index', $data);
});

Route::get('/data-presensi/detail', function () {
    $data = array(
        'title' => 'Detail Data Presensi Karyawan 1',
        'menu' => 'data-presensi',
        'submenu' => ''
    );
    return view('menu.data-presensi.detail', $data);
});

Route::get('/data-gaji', function () {
    $data = array(
        'title' => 'Data Gaji Karyawan',
        'menu' => 'data-gaji',
        'submenu' => ''
    );
    return view('menu.data-gaji.index', $data);
});

Route::get('/data-gaji/detail-periode', function () {
    $data = array(
        'title' => 'Data Gaji Periode 26 Januari 2022 - 25 Februari 2022',
        'menu' => 'data-gaji',
        'submenu' => ''
    );
    return view('menu.data-gaji.detail-periode', $data);
});

Route::get('/data-gaji/detail-karyawan', function () {
    $data = array(
        'title' => 'Data Gaji Karyawan 1 <br> Periode 26 Januari 2022 - 25 Februari 2022',
        'menu' => 'data-gaji',
        'submenu' => ''
    );
    return view('menu.data-gaji.detail-karyawan', $data);
});

Route::get('/jadwal-karyawan', function () {
    $data = array(
        'title' => 'Jadwal Karyawan Unit 1',
        'menu' => 'jadwal-karyawan',
        'submenu' => ''
    );
    return view('menu.jadwal-karyawan.index', $data);
});

Route::get('/data-cuti', function () {
    $data = array(
        'title' => 'Data Cuti',
        'menu' => 'data-cuti',
        'submenu' => ''
    );
    return view('menu.data-cuti.index', $data);
});